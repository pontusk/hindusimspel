import React from 'react';
import { render } from 'react-dom';
import Favicon from 'react-favicon';
import ReactGA from 'react-ga';
import './stylesheets/styles.scss';
import App from './components/App';
import { soundManager } from '../node_modules/soundmanager2/script/soundmanager2';
import favicon from './images/favicon.png';

ReactGA.initialize('UA-114678337-1');

soundManager.setup({
  ignoreMobileRestrictions: true
});

render(
  <div style={{ height: '100%' }}>
    <Favicon url={favicon} />
    <App />
  </div>,
  document.getElementById('react-container')
);
