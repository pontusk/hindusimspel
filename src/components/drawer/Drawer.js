import React from 'react';
import PropTypes from 'prop-types';
import animate from '../utilities/animations';
import DrawerDiv from './DrawerDiv';
import Handle from './Handle';
import Attribute from './Attribute';
import global from '../utilities/globalStuff';

class Drawer extends React.Component {
  // Method to render the icons for all the attributes
  static renderAttributes() {
    return global.attrs.map(attr => (
      <Attribute key={attr.toString()} className={`attr-${attr} item`} />
    ));
  }

  constructor(props) {
    super(props);
    // Binding 'this' before passing methods on
    this.smileOn = this.smileOn.bind(this);
    this.smileOff = this.smileOff.bind(this);
    this.pointOn = this.pointOn.bind(this);
    this.pointOff = this.pointOff.bind(this);
    this.attr1On = this.attr1On.bind(this);
    this.attr1Off = this.attr1Off.bind(this);
    this.attr2On = this.attr2On.bind(this);
    this.attr2Off = this.attr2Off.bind(this);
    this.attr3On = this.attr3On.bind(this);
    this.attr3Off = this.attr3Off.bind(this);
    this.attr4On = this.attr4On.bind(this);
    this.attr4Off = this.attr4Off.bind(this);
    this.attr5On = this.attr5On.bind(this);
    this.attr5Off = this.attr5Off.bind(this);
    this.attr6On = this.attr6On.bind(this);
    this.attr6Off = this.attr6Off.bind(this);
    this.attr7On = this.attr7On.bind(this);
    this.attr7Off = this.attr7Off.bind(this);
    this.attr8On = this.attr8On.bind(this);
    this.attr8Off = this.attr8Off.bind(this);
    this.attr9On = this.attr9On.bind(this);
    this.attr9Off = this.attr9Off.bind(this);
    this.attr10On = this.attr10On.bind(this);
    this.attr10Off = this.attr10Off.bind(this);
    this.infoOn = this.infoOn.bind(this);
    this.switchSound = this.switchSound.bind(this);
  }
  componentDidMount() {
    // Putting methods in variables to pass them on
    const thatSmileOn = this.smileOn;
    const thatSmileOff = this.smileOff;
    const thatPointOn = this.pointOn;
    const thatPointOff = this.pointOff;
    const thatAttr1On = this.attr1On;
    const thatAttr1Off = this.attr1Off;
    const thatAttr2On = this.attr2On;
    const thatAttr2Off = this.attr2off;
    const thatAttr3On = this.attr3On;
    const thatAttr3Off = this.attr3off;
    const thatAttr4On = this.attr4On;
    const thatAttr4Off = this.attr4off;
    const thatAttr5On = this.attr5On;
    const thatAttr5Off = this.attr5off;
    const thatAttr6On = this.attr6On;
    const thatAttr6Off = this.attr6off;
    const thatAttr7On = this.attr7On;
    const thatAttr7Off = this.attr7off;
    const thatAttr8On = this.attr8On;
    const thatAttr8Off = this.attr8Off;
    const thatAttr9On = this.attr9On;
    const thatAttr9Off = this.attr9off;
    const thatAttr10On = this.attr10On;
    const thatAttr10Off = this.attr10Off;
    const thatInfoOn = this.infoOn;
    const thatSwitchSound = this.switchSound;
    const logAttempts = this.props.incrementAttempts;
    // ES6 deconstruction of name prop
    const { name } = this.props;
    // Animate the drawer and pass on all the methods
    if (window.matchMedia('(max-width: 768px)').matches) {
      animate.draggableDrawer();
    }
    animate.draggableItems(
      thatSmileOn,
      thatSmileOff,
      thatPointOn,
      thatPointOff,
      thatAttr1On,
      thatAttr1Off,
      thatAttr2On,
      thatAttr2Off,
      thatAttr3On,
      thatAttr3Off,
      thatAttr4On,
      thatAttr4Off,
      thatAttr5On,
      thatAttr5Off,
      thatAttr6On,
      thatAttr6Off,
      thatAttr7On,
      thatAttr7Off,
      thatAttr8On,
      thatAttr8Off,
      thatAttr9On,
      thatAttr9Off,
      thatAttr10On,
      thatAttr10Off,
      thatInfoOn,
      thatSwitchSound,
      logAttempts,
      name
    );
  }

  // Here are all the methods to change state
  smileOn() {
    this.props.updateState({ isSmiling: true });
  }
  smileOff() {
    this.props.updateState({ isSmiling: false });
  }
  pointOn() {
    this.props.updateState({ isPointing: true });
  }
  pointOff() {
    this.props.updateState({ isPointing: false });
  }
  attr1On() {
    this.props.updateState({ hasAttr1: true });
  }
  attr1Off() {
    this.props.updateState({ hasAttr1: false });
  }
  attr2On() {
    this.props.updateState({ hasAttr2: true });
  }
  attr2Off() {
    this.props.updateState({ hasAttr2: false });
  }
  attr3On() {
    this.props.updateState({ hasAttr3: true });
  }
  attr3Off() {
    this.props.updateState({ hasAttr3: false });
  }
  attr4On() {
    this.props.updateState({ hasAttr4: true });
  }
  attr4Off() {
    this.props.updateState({ hasAttr4: false });
  }
  attr5On() {
    this.props.updateState({ hasAttr5: true });
  }
  attr5Off() {
    this.props.updateState({ hasAttr5: false });
  }
  attr6On() {
    this.props.updateState({ hasAttr6: true });
  }
  attr6Off() {
    this.props.updateState({ hasAttr6: false });
  }
  attr7On() {
    this.props.updateState({ hasAttr7: true });
  }
  attr7Off() {
    this.props.updateState({ hasAttr7: false });
  }
  attr8On() {
    this.props.updateState({ hasAttr8: true });
  }
  attr8Off() {
    this.props.updateState({ hasAttr8: false });
  }
  attr9On() {
    this.props.updateState({ hasAttr9: true });
  }
  attr9Off() {
    this.props.updateState({ hasAttr9: false });
  }
  attr10On() {
    this.props.updateState({ hasAttr10: true });
  }
  attr10Off() {
    this.props.updateState({ hasAttr10: false });
  }
  infoOn(attr) {
    this.props.updateState({ showInfo: attr });
  }
  switchSound(sound) {
    this.props.switchSound(sound);
  }
  render() {
    return (
      <section className="drawer-container">
        <div className="drawer-bounds">
          <DrawerDiv />
          <div className="item-container">
            <Handle />
            <div className="draggables-container">{Drawer.renderAttributes()}</div>
            {/* draggables-container */}
          </div>
          {/* item-container */}
        </div>
        {/* drawer-bounds */}
      </section> // drawer-container
    );
  }
}

Drawer.defaultProps = {
  switchSound: () => console.log('prop is missing'),
  name: 'Shiva',
  updateState: () => console.log('prop is missing'),
  incrementAttempts: () => console.log('prop is missing')
};

Drawer.propTypes = {
  switchSound: PropTypes.func,
  name: PropTypes.oneOf(global.gods()),
  updateState: PropTypes.func,
  incrementAttempts: PropTypes.func
};

export { Drawer as default };
