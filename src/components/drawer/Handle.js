import React from 'react';

const Handle = () => (
  <div className="draggable-handle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.379 44">
      <path
        fill="none"
        strokeMiterlimit="10"
        strokeWidth="2"
        d="M2.723 0v44M9.723 0v44M16.723 0v44"
      />
    </svg>
  </div>
);

export { Handle as default };
