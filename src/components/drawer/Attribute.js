import React from 'react';
import PropTypes from 'prop-types';

const Attribute = ({ className }) => (
  <div
    className={className}
    alt={className
      .split('-')
      .pop()
      .split(' ')
      .shift()
      .trim()}
  />
);

Attribute.defaultProps = {
  className: 'item'
};

Attribute.propTypes = {
  className: PropTypes.string
};

export { Attribute as default };
