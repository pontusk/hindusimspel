import React from 'react';
import PropTypes from 'prop-types';
// import TransitionGroup from 'react-addons-transition-group';
import color from '../utilities/colors';
import global from '../utilities/globalStuff';

import GaneshaPattern from './bodyparts/patterns/GaneshaPattern';
import Legs from './bodyparts/legs/Legs';
import Arms from './bodyparts/arms/Arms';
import Body from './bodyparts/body/Body';
import Head from './bodyparts/head/Head';
import Attributes from './attributes/Attributes';

class God extends React.Component {
  componentDidMount() {
    const { finishedGods, updateState, name } = this.props;
    if (finishedGods.indexOf(name) === -1) {
      updateState({ showInfo: 'god' });
    }
  }
  classN() {
    // adds some character specific styles
    let classN;
    const { styled, name } = this.props;
    if (styled) {
      if (name === 'Ganesha') {
        classN = 'ganesha-style';
      } else if (name === 'Parvati' || name === 'Lakshmi' || name === 'Kali') {
        classN = 'godess-style';
      } else {
        classN = 'god-style';
      }
    } else {
      classN = undefined;
    }
    return classN;
  }
  render() {
    const {
      name,
      isPointing,
      isSmiling,
      isWinking,
      hasAttr1,
      hasAttr2,
      hasAttr3,
      hasAttr4,
      hasAttr5,
      hasAttr6,
      hasAttr7,
      hasAttr8,
      hasAttr9,
      hasAttr10,
      soundPlaying
    } = this.props;
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox={
          // larger viewbox for the godesses
          name === 'Parvati' || name === 'Lakshmi' || name === 'Kali'
            ? '14 5 930 850'
            : '23 5 830 850'
        }
        id="god"
        alt={name}
        className={this.classN()}
      >
        {name === 'Ganesha' && <GaneshaPattern />}
        <Legs name={name} fill={color.skinColor(name)[0]} stroke={color.skinColor(name)[1]} />
        <Arms
          name={name}
          isPointing={isPointing}
          fill={color.skinColor(name)[0]}
          stroke={color.skinColor(name)[1]}
        />
        <Body
          name={name}
          fill={color.skinColor(name)[0]}
          stroke={color.skinColor(name)[1]}
          hasAttr6={hasAttr6}
        />
        <Head
          name={name}
          fill={color.skinColor(name)[0]}
          stroke={color.skinColor(name)[1]}
          noseStroke={color.skinColor(name)[2]}
          eyeFill={color.skinColor(name)[3]}
          noseFill={color.skinColor(name)[5]}
          isSmiling={isSmiling}
          isWinking={isWinking}
          isPointing={isPointing}
        />
        <Attributes
          name={name}
          hasAttr1={hasAttr1}
          hasAttr2={hasAttr2}
          hasAttr3={hasAttr3}
          hasAttr4={hasAttr4}
          hasAttr5={hasAttr5}
          hasAttr6={hasAttr6}
          hasAttr7={hasAttr7}
          hasAttr8={hasAttr8}
          hasAttr9={hasAttr9}
          hasAttr10={hasAttr10}
          soundPlaying={soundPlaying}
        />
      </svg>
    );
  }
}

God.defaultProps = {
  soundPlaying: false,
  name: 'Shiva',
  styled: false,
  isPointing: false,
  isSmiling: false,
  isWinking: false,
  hasAttr1: false, // Toggles attribute specific to the God in question
  hasAttr2: false,
  hasAttr3: false,
  hasAttr4: false,
  hasAttr5: false,
  hasAttr6: false,
  hasAttr7: false,
  hasAttr8: false,
  hasAttr9: false,
  hasAttr10: false,
  updateState: () => {},
  showInfo: 'closed',
  finishedGods: []
};

God.propTypes = {
  soundPlaying: PropTypes.bool,
  name: PropTypes.oneOf(global.gods()),
  styled: PropTypes.bool,
  isPointing: PropTypes.bool,
  isSmiling: PropTypes.bool,
  isWinking: PropTypes.bool,
  hasAttr1: PropTypes.bool,
  hasAttr2: PropTypes.bool,
  hasAttr3: PropTypes.bool,
  hasAttr4: PropTypes.bool,
  hasAttr5: PropTypes.bool,
  hasAttr6: PropTypes.bool,
  hasAttr7: PropTypes.bool,
  hasAttr8: PropTypes.bool,
  hasAttr9: PropTypes.bool,
  hasAttr10: PropTypes.bool,
  updateState: PropTypes.func,
  showInfo: PropTypes.oneOf(global.infoTypes()),
  finishedGods: PropTypes.arrayOf(PropTypes.string)
};

export { God as default };
