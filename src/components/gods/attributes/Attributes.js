import React from 'react';
import PropTypes from 'prop-types';
import Trident from './Trident';
import Bowl from './Bowl';
import Axe from './Axe';
import Snake from './snake/Snake';
import Drum from './Drum';
import Rope from './Rope';
import Chakra from './Chakra';
import ParvAxe from './parvati/ParvAxe';
import ParvClub from './parvati/ParvClub';
import ParvLotus from './parvati/ParvLotus';
import ParvTrident from './parvati/ParvTrident';
import ParvShell from './parvati/ParvShell';
import ParvKnot from './parvati/ParvKnot';
import ParvDrum from './parvati/ParvDrum';
import ParvRope from './parvati/ParvRope';
import ParvBowl from './parvati/ParvBowl';
import Lotus from './Lotus';
import Money from './Money';
import Skull from './Skull';
import Club from './Club';
import Shell from './Shell';
import VishnuLotus from './VishnuLotus';
import KaliLotus from './KaliLotus';
import GaneshaLotus from './GaneshaLotus';
import KaliTrident from './KaliTrident';
import global from '../../utilities/globalStuff';

const Attributes = ({
  name,
  hasAttr1,
  hasAttr2,
  hasAttr3,
  hasAttr4,
  hasAttr5,
  hasAttr6,
  hasAttr7,
  hasAttr8,
  hasAttr9,
  hasAttr10,
  soundPlaying
}) => (
  // Renders attributes depending on current state and what god
  <g>
    {/* Shiva */}
    {name === 'Shiva' && hasAttr1 && <Snake name={name} soundPlaying={soundPlaying} />}
    {name === 'Shiva' && hasAttr2 && <Trident />}
    {name === 'Shiva' && hasAttr3 && <Drum />}
    {/* Ganesha */}
    {name === 'Ganesha' && hasAttr1 && <Bowl />}
    {name === 'Ganesha' && hasAttr2 && <Axe />}
    {name === 'Ganesha' && hasAttr3 && <Rope />}
    {name === 'Ganesha' && hasAttr4 && <GaneshaLotus />}
    {/* Pravati */}
    {name === 'Parvati' && hasAttr1 && <Chakra name={name} />}
    {name === 'Parvati' && hasAttr2 && <ParvAxe />}
    {name === 'Parvati' && hasAttr3 && <ParvClub />}
    {name === 'Parvati' && hasAttr4 && <ParvLotus />}
    {name === 'Parvati' && hasAttr8 && <ParvDrum />}
    {name === 'Parvati' && hasAttr5 && <ParvTrident />}
    {name === 'Parvati' && hasAttr6 && <ParvShell />}
    {name === 'Parvati' && hasAttr7 && <Snake name={name} soundPlaying={soundPlaying} />}
    {name === 'Parvati' && hasAttr8 && <ParvKnot />}
    {name === 'Parvati' && hasAttr9 && <ParvRope />}
    {name === 'Parvati' && hasAttr10 && <ParvBowl />}
    {/* Lakshmi */}
    {name === 'Lakshmi' && hasAttr1 && <Lotus />}
    {name === 'Lakshmi' && hasAttr2 && <Money />}
    {/* Kali */}
    {name === 'Kali' && hasAttr1 && <ParvAxe />}
    {name === 'Kali' && hasAttr2 && <ParvClub />}
    {name === 'Kali' && hasAttr3 && <Chakra name={name} />}
    {name === 'Kali' && hasAttr4 && <KaliTrident />}
    {name === 'Kali' && hasAttr5 && <ParvShell />}
    {name === 'Kali' && hasAttr6 && <Skull />}
    {name === 'Kali' && hasAttr7 && <KaliLotus />}
    {name === 'Kali' && hasAttr8 && <Snake name={name} soundPlaying={soundPlaying} />}
    {/* Vishnu */}
    {name === 'Vishnu' && hasAttr1 && <Chakra name={name} />}
    {name === 'Vishnu' && hasAttr2 && <Club />}
    {name === 'Vishnu' && hasAttr3 && <Shell />}
    {name === 'Vishnu' && hasAttr4 && <VishnuLotus />}
  </g>
);

Attributes.defaultProps = {
  soundPlaying: false,
  name: 'Shiva',
  hasAttr1: false, // Toggles attribute specific to the God in question
  hasAttr2: false,
  hasAttr3: false,
  hasAttr4: false,
  hasAttr5: false,
  hasAttr6: false,
  hasAttr7: false,
  hasAttr8: false,
  hasAttr9: false,
  hasAttr10: false
};

Attributes.propTypes = {
  soundPlaying: PropTypes.bool,
  name: PropTypes.oneOf(global.gods()),
  hasAttr1: PropTypes.bool,
  hasAttr2: PropTypes.bool,
  hasAttr3: PropTypes.bool,
  hasAttr4: PropTypes.bool,
  hasAttr5: PropTypes.bool,
  hasAttr6: PropTypes.bool,
  hasAttr7: PropTypes.bool,
  hasAttr8: PropTypes.bool,
  hasAttr9: PropTypes.bool,
  hasAttr10: PropTypes.bool
};

export { Attributes as default };
