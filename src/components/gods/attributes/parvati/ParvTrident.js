import React from 'react';

const ParvTrident = () => (
  <g id="trident">
    <path
      fill="#FBB03B"
      d="M729.507,50.129c-9.356,36.86-9.348,73.586-6.973,97.461c2.592,26.042,0.344,34.931-8.238,43.438 c-8.585,8.506-29.646,0.292-38.611,0.38l-1.083-2.885l0.113,11.636c0,0,3.331-1.004,11.69,3.807 c9.542,5.491,29.44,13.423,39.885-5.876c8.608-15.907,8.569-44.093,2.903-69.752C723.018,100.362,728.2,59.696,729.507,50.129z"
    />
    <path
      fill="#FCDDB4"
      d="M714.296,191.028c8.582-8.507,10.83-17.396,8.238-43.438c-2.375-23.875-2.384-60.601,6.973-97.461 c0.184-1.34,0.295-2.074,0.295-2.074s-17.419,48.486-17.181,83.215c0.17,24.833,1.222,41.301-6.26,47.676 c-3.838,3.27-11.815,3.877-29.82,3.49l0.06,6.068l0.085,2.895C685.65,191.31,705.711,199.534,714.296,191.028z"
    />
    <path
      fill="#FCDDB4"
      d="M605.638,51.349c10.081,36.669,10.794,73.389,8.89,97.304c-2.077,26.088,0.344,34.932,9.095,43.268 c8.748,8.335,29.742-0.292,38.708-0.381l1.128-2.906l0.113,11.636c0,0-3.447-0.938-11.711,4.037 c-9.431,5.678-29.221,14.001-40.043-5.089c-8.922-15.735-9.458-43.916-4.301-69.683 C613.141,101.444,607.132,60.889,605.638,51.349z"
    />
    <path
      fill="#FBB03B"
      d="M623.622,191.92c-8.751-8.336-11.172-17.179-9.095-43.268c1.904-23.916,1.191-60.635-8.89-97.304 c-0.21-1.336-0.335-2.067-0.335-2.067s18.37,48.134,18.814,82.859c0.32,24.832-0.31,41.316,7.298,47.543 c3.9,3.193,11.988,3.642,29.982,2.901l0.059,6.067l-0.126,2.896C652.364,191.637,632.37,200.256,623.622,191.92z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.498,182.437 c17.576,0.391,26.004-0.221,29.842-3.49c7.481-6.375,6.451-22.844,6.281-47.676c-0.238-34.729,17.181-83.215,17.181-83.215 s-7.405,48.388-0.364,80.279c5.665,25.659,5.95,53.843-2.656,69.75c-10.446,19.301-29.856,11.362-39.397,5.872 c-8.36-4.811-10.711-3.816-10.711-3.816"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M661.476,182.589 c-17.564,0.737-25.857,0.286-29.76-2.908c-7.608-6.227-6.899-22.712-7.221-47.544c-0.444-34.726-18.813-82.86-18.813-82.86 s8.355,48.232,1.944,80.256c-5.159,25.766-4.89,53.95,4.03,69.686c10.822,19.09,30.073,10.771,39.505,5.094 c8.264-4.975,10.489-4.013,10.489-4.013"
    />
    <path
      fill="#FBB03B"
      d="M679.217,172.43c-0.025-2.816-2.776-3.729-2.776-3.729s1.848-1.217,1.842-1.896 c-0.007-0.679-1.912-1.859-1.912-1.859l-0.074-7.511c0,0,1.865-1.218,1.858-1.896c-0.006-0.679-1.896-1.859-1.896-1.859 l-0.055-5.634c0,0,3.736-3.692,3.719-5.57c-0.019-1.878-3.952-5.495-3.952-5.495l-14.333,0.141c0,0-4.253,4.012-4.238,5.575 c0.017,1.565,3.804,5.497,3.804,5.497l0.057,5.634c0,0-1.864,1.331-1.859,1.896c0.006,0.564,1.896,1.86,1.896,1.86l0.075,7.511 c0,0-1.862,1.332-1.855,1.896c0.005,0.565,1.898,1.859,1.898,1.859s-2.878,1.281-2.854,3.784c0.024,2.426,2.93,3.728,2.93,3.728 s-1.871,1.35-1.866,1.914c0.007,0.564,1.889,1.877,1.889,1.877h-0.011l0.233,22.837c0,0-1.892,1.521-1.885,2.086 c0.005,0.564,1.865,2.053,1.865,2.053l0.068-0.002l0.106,10.718c0,0-1.861,1.33-1.855,1.895c0.005,0.564,1.899,1.86,1.899,1.86 s-2.879,1.279-2.854,3.784c0.023,2.425,2.929,3.727,2.929,3.727s-1.861,1.46-1.855,2.025c0.005,0.564,1.896,1.988,1.896,1.988 l15.023-0.148c0,0,1.864-1.346,1.858-2.025c-0.007-0.678-1.897-1.924-1.897-1.924s2.731-0.921,2.703-3.815 c-0.028-2.816-2.777-3.744-2.777-3.744s1.848-1.226,1.842-1.905c-0.007-0.678-1.912-1.778-1.912-1.778l-0.107-10.631h-0.043 c0,0,1.887-1.502,1.88-2.181c-0.008-0.679-1.877-2.056-1.877-2.056l-0.225-22.828l0.034-0.009c0,0,1.866-1.273,1.859-1.952 c-0.008-0.678-1.896-1.887-1.896-1.887S679.246,175.325,679.217,172.43z"
    />
    <path
      fill="#FBB03B"
      d="M666.94,19.876l1.154,117.183l7.875-0.078c0,0-4.731-18.833,5.007-47.1 C690.382,62.575,667.967,22.558,666.94,19.876z"
    />
    <path
      fill="#FCDDB4"
      d="M667.081,19.729c0,0-22.345,42.34-12.055,70.408c10.293,28.069,6.61,46.985,6.61,46.985l6.822-0.067 l-0.364,0.004L666.94,19.876C666.939,19.782,667.081,19.729,667.081,19.729z"
    />
    <rect
      x="665.566"
      y="137.071"
      transform="matrix(1 -.0099 .0099 1 -1.804 6.632)"
      fill="#FCDDB4"
      width="3"
      height="96"
    />
    <rect
      x="661.241"
      y="148.119"
      transform="matrix(1 -.0099 .0099 1 -1.459 6.615)"
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      width="15.023"
      height="5.634"
    />
    <rect
      x="661.343"
      y="157.508"
      transform="matrix(1 -.0099 .0099 1 -1.56 6.617)"
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      width="15.023"
      height="7.512"
    />
    <rect
      x="661.563"
      y="180.043"
      transform="matrix(1 -.0098 .0098 1 -1.853 6.598)"
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      width="15.023"
      height="22.86"
    />
    <rect
      x="661.845"
      y="206.658"
      transform="matrix(1 -.0098 .0098 1 -2.057 6.602)"
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      width="15.023"
      height="11.111"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.236,148.046l-15.023,0.147 c0,0-3.794-4.032-3.81-5.597c-0.015-1.563,4.232-5.675,4.232-5.675l14.332-0.141c0,0,3.95,3.717,3.97,5.595 C679.956,144.253,676.236,148.046,676.236,148.046z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.33,157.435l-15.023,0.147 c0,0-1.891-1.295-1.896-1.859c-0.005-0.564,1.859-1.896,1.859-1.896l15.022-0.148c0,0,1.891,1.182,1.896,1.86 C678.195,156.217,676.33,157.435,676.33,157.435z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.44,168.701l-15.023,0.148 c0,0-1.891-1.294-1.896-1.86c-0.006-0.565,1.859-1.896,1.859-1.896l15.022-0.148c0,0,1.89,1.182,1.896,1.86 C678.306,167.483,676.44,168.701,676.44,168.701z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.551,179.968l-15.021,0.148 c0,0-1.892-1.294-1.897-1.859c-0.004-0.565,1.858-1.896,1.858-1.896l15.023-0.148c0,0,1.891,1.181,1.896,1.859 C678.418,178.751,676.551,179.968,676.551,179.968z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.514,176.213c0,0,2.732-0.888,2.703-3.783 c-0.025-2.816-2.776-3.729-2.776-3.729l-15.023,0.148c0,0-2.88,1.28-2.856,3.784c0.024,2.426,2.93,3.727,2.93,3.727 L676.514,176.213z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.96,221.45l-15.023,0.148 c0,0-1.89-1.294-1.896-1.859c-0.005-0.564,1.86-1.896,1.86-1.896l15.022-0.148c0,0,1.891,1.181,1.896,1.859 C678.825,220.232,676.96,221.45,676.96,221.45z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M676.736,206.584l-15.022,0.148 c0,0-1.891-1.294-1.896-1.86c-0.006-0.564,1.857-1.896,1.857-1.896l15.022-0.148c0,0,1.89,1.181,1.897,1.859 C678.603,205.367,676.736,206.584,676.736,206.584z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M677.071,232.717l-15.022,0.147 c0,0-1.892-1.294-1.897-1.859c-0.006-0.565,1.858-1.896,1.858-1.896l15.023-0.148c0,0,1.89,1.181,1.897,1.86 C678.938,231.499,677.071,232.717,677.071,232.717z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M677.033,228.961c0,0,2.732-0.889,2.705-3.783 c-0.029-2.816-2.778-3.729-2.778-3.729l-15.023,0.148c0,0-2.88,1.279-2.856,3.784c0.024,2.424,2.93,3.727,2.93,3.727 L677.033,228.961z"
    />
    <path
      fill="none"
      stroke="#C68228"
      strokeWidth="3"
      strokeMiterlimit="10"
      d="M667.305,19.726c0,0-22.458,42.241-12.169,70.31 c10.294,28.069,6.5,46.886,6.5,46.886l6.821-0.067l7.511-0.074c0,0-4.509-18.736,5.229-47.001 C690.933,61.514,667.305,19.726,667.305,19.726z"
    />
    <g>
      <path
        fill="#490A1C"
        d="M676.626,232.722l-14.286,0.141l0.727,59.93c0.806-0.088,1.621-0.167,2.449-0.233 c1.411-0.112,2.846-0.193,4.284-0.245c2.557-0.092,5.11-0.091,7.548-0.015L676.626,232.722z"
      />
      <path
        fill="#701131"
        d="M664.756,234.318l0.76,58.241c1.411-0.112,2.846-0.193,4.284-0.245l-0.758-58.038L664.756,234.318z"
      />
      <path
        fill="#490A1C"
        d="M678.109,359.501c-2.56-0.047-4.702-0.039-5.728,0.078l-0.295,0.033c-0.473,0.053-0.964,0.107-1.464,0.161 c-1.363,0.147-2.812,0.293-4.281,0.427c-0.835,0.076-1.674,0.148-2.508,0.213l3.869,324.107c0.948,0.522,1.894,1.044,2.824,1.563 c1.464,0.817,2.899,1.63,4.317,2.44c2.489,1.423,4.901,2.833,7.236,4.229L678.109,359.501z"
      />
      <path
        fill="#701131"
        d="M666.342,360.201l4.186,325.884c1.464,0.817,2.899,1.63,4.317,2.44l-4.222-328.752 C669.26,359.92,667.812,360.066,666.342,360.201z"
      />
      <line
        fill="none"
        stroke="#000"
        strokeWidth="2"
        strokeMiterlimit="10"
        x1="682.081"
        y1="692.754"
        x2="678.109"
        y2="359.501"
      />
      <line
        fill="none"
        stroke="#000"
        strokeWidth="2"
        strokeMiterlimit="10"
        x1="663.834"
        y1="360.414"
        x2="667.703"
        y2="684.521"
      />
      <line
        fill="none"
        stroke="#000"
        strokeWidth="2"
        strokeMiterlimit="10"
        x1="662.34"
        y1="232.862"
        x2="663.066"
        y2="292.792"
      />
      <line
        fill="none"
        stroke="#000"
        strokeWidth="2"
        strokeMiterlimit="10"
        x1="677.348"
        y1="292.299"
        x2="676.626"
        y2="232.722"
      />
    </g>
  </g>
);

export { ParvTrident as default };
