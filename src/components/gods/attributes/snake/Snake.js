import React from 'react';
import PropTypes from 'prop-types';
import TweenMax from 'gsap';
import Sound from 'react-sound';
import animate from '../../../utilities/animations';
import ShivaSnake from './ShivaSnake';
import ParvSnake from './ParvSnake';
import KaliSnake from './KaliSnake';
import global from '../../../utilities/globalStuff';
import hiss from '../../../../sounds/hiss.mp3';

class Snake extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      snakePlaying: true
    };
    this.handleFinishedPlaying = this.handleFinishedPlaying.bind(this);
  }
  componentDidMount() {
    animate.idleSnake();
  }
  componentWillUnmount() {
    TweenMax.killTweensOf(
      '#snake-eyes-open, #snake-eyes-closed, #snake-tongue-inv, #snake-tongue-default, #snake-tongue-short'
    );
  }
  snakePlay() {
    let status = Sound.status.PLAYING;
    if (!this.state.snakePlaying) {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  handleFinishedPlaying() {
    this.setState({ snakePlaying: false });
  }
  render() {
    const { name, soundPlaying } = this.props;
    return (
      <g>
        {name === 'Shiva' && <ShivaSnake />}
        {name === 'Parvati' && <ParvSnake />}
        {name === 'Kali' && <KaliSnake />}
        {soundPlaying && (
          <Sound
            url={hiss}
            autoPlay
            playStatus={this.snakePlay()}
            onFinishedPlaying={this.handleFinishedPlaying}
          />
        )}
      </g>
    );
  }
}

Snake.defaultProps = {
  soundPlaying: false,
  name: 'Shiva'
};
Snake.propTypes = {
  soundPlaying: PropTypes.bool,
  name: PropTypes.oneOf(global.gods())
};
export { Snake as default };
