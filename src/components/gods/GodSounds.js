import React from 'react';
import PropTypes from 'prop-types';
import Sound from 'react-sound';
import maleApproving from '../../sounds/male-approving.mp3';
import disapproving from '../../sounds/disapproving.mp3';
import femaleApproving from '../../sounds/female-approving.mp3';
import ganeshaTasty from '../../sounds/ganesha-tasty.mp3';
import kaliApproving from '../../sounds/kali-approving.mp3';
import kaliDisapproving from '../../sounds/kali-disapproving.mp3';
import global from '../utilities/globalStuff';

class GodSounds extends React.Component {
  constructor(props) {
    super(props);
    this.handleSongFinishedPlaying = this.handleSongFinishedPlaying.bind(this);
  }
  maleApprovingPlay() {
    const { currentSound, soundPlaying } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'male-approving' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  disapprovingPlay() {
    const { currentSound, soundPlaying } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'disapproving' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  femaleApprovingPlay() {
    const { currentSound, soundPlaying } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'female-approving' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  ganeshaTastyPlay() {
    const { currentSound, soundPlaying } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'ganesha-tasty' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  kaliApprovingPlay() {
    const { currentSound, soundPlaying } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'kali-approving' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  kaliDisapprovingPlay() {
    const { currentSound, soundPlaying } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'kali-disapproving' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  handleSongFinishedPlaying() {
    this.props.switchSound('none');
  }
  render() {
    return (
      <div>
        <Sound
          url={maleApproving}
          autoLoad
          playStatus={this.maleApprovingPlay()}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        <Sound
          url={disapproving}
          autoLoad
          playStatus={this.disapprovingPlay()}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        <Sound
          url={femaleApproving}
          autoLoad
          playStatus={this.femaleApprovingPlay()}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        <Sound
          url={ganeshaTasty}
          autoLoad
          playStatus={this.ganeshaTastyPlay()}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        <Sound
          url={kaliApproving}
          autoLoad
          playStatus={this.kaliApprovingPlay()}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        <Sound
          url={kaliDisapproving}
          autoLoad
          playStatus={this.kaliDisapprovingPlay()}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
      </div>
    );
  }
}

GodSounds.defaultProps = {
  switchSound: () => console.log('prop is missing'),
  playSound: () => console.log('prop is missing'),
  stopSound: () => console.log('prop is missing'),
  soundPlaying: true,
  name: 'Shiva',
  currentSound: 'none'
};

GodSounds.propTypes = {
  switchSound: PropTypes.func,
  playSound: PropTypes.func,
  stopSound: PropTypes.func,
  soundPlaying: PropTypes.bool,
  name: PropTypes.oneOf(global.gods()),
  currentSound: PropTypes.string
};

export { GodSounds as default };
