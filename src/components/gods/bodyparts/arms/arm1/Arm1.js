import React from 'react';
import PropTypes from 'prop-types';

import ShivaArm1main from './ShivaArm1main';
import Arm1point from './Arm1point';
import ShivaArm1open from './ShivaArm1open';
import GaneshaArm1main from './GaneshaArm1main';
import GaneshaArm1open from './GaneshaArm1open';
import VishnuArm1main from './VishnuArm1main';
import VishnuArm1open from './VishnuArm1open';
import global from '../../../../utilities/globalStuff';

class Arm1 extends React.Component {
  renderArm() {
    // Renders the right arm depending on god
    let arm1;
    const { name, isPointing, fill, stroke } = this.props;
    if (name === 'Shiva') {
      if (isPointing) {
        arm1 = (
          <g id="arm1">
            <ShivaArm1main fill={fill} stroke={stroke} />
            <Arm1point fill={fill} stroke={stroke} name={name} />
          </g>
        );
      } else {
        arm1 = (
          <g id="arm1">
            <ShivaArm1main fill={fill} stroke={stroke} />
            <ShivaArm1open fill={fill} stroke={stroke} />
          </g>
        );
      }
    } else if (name === 'Ganesha') {
      if (isPointing) {
        arm1 = (
          <g id="arm1">
            <GaneshaArm1main fill={fill} stroke={stroke} />
            <Arm1point fill={fill} stroke={stroke} name={name} />
          </g>
        );
      } else {
        arm1 = (
          <g id="arm1">
            <GaneshaArm1main fill={fill} stroke={stroke} />
            <GaneshaArm1open fill={fill} stroke={stroke} />
          </g>
        );
      }
    } else if (name === 'Vishnu') {
      if (isPointing) {
        arm1 = (
          <g id="arm1">
            <VishnuArm1main fill={fill} stroke={stroke} />
            <Arm1point fill={fill} stroke={stroke} name={name} />
          </g>
        );
      } else {
        arm1 = (
          <g id="arm1">
            <VishnuArm1main fill={fill} stroke={stroke} />
            <VishnuArm1open fill={fill} stroke={stroke} />
          </g>
        );
      }
    }
    return arm1;
  }
  render() {
    return this.renderArm();
  }
}

Arm1.defaultProps = {
  isPointing: false,
  fill: '',
  stroke: '',
  name: 'Shiva'
};

Arm1.propTypes = {
  isPointing: PropTypes.bool,
  fill: PropTypes.string,
  stroke: PropTypes.string,
  name: PropTypes.oneOf(global.gods())
};

export { Arm1 as default };
