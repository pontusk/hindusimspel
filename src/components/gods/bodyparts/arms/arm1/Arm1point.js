import React from 'react';
import { TweenMax, TimelineMax } from 'gsap';
import PropTypes from 'prop-types';
import animate from '../../../../utilities/animations';
import global from '../../../../utilities/globalStuff';
import ShivaArm1point from './ShivaArm1point';
import GaneshaArm1point from './GaneshaArm1point';
import VishnuArm1point from './VishnuArm1point';

class Arm1point extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      const tl1 = new TimelineMax();
      const tl2 = new TimelineMax();
      tl1.to('#pupil-right, #pupil-left', 0, { x: 0 });
      tl2.to('#eyes-default-closed', 0, { opacity: 1 });
      TweenMax.killTweensOf('#pupil-right, #pupil-left, #eyes-default-closed');
      animate.wagFinger(this.props.name);
    }, 200);
  }
  componentWillUnmount() {
    TweenMax.killTweensOf('#arm1-point');
    animate.idleEyes();
  }
  render() {
    const { name, stroke, fill } = this.props;
    return (
      <g>
        {name === 'Shiva' && <ShivaArm1point stroke={stroke} fill={fill} />}
        {name === 'Ganesha' && <GaneshaArm1point stroke={stroke} fill={fill} />}
        {name === 'Vishnu' && <VishnuArm1point stroke={stroke} fill={fill} />}
      </g>
    );
  }
}

Arm1point.defaultProps = {
  fill: '',
  stroke: '',
  name: 'Shiva'
};

Arm1point.propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  name: PropTypes.oneOf(global.gods())
};

export { Arm1point as default };
