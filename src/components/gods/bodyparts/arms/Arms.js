import React from 'react';
import PropTypes from 'prop-types';
import GaneshaArms from './GaneshaArms';
import ShivaArms from './ShivaArms';
import ParvatiArms from './ParvatiArms';
import LakshmiArms from './LakshmiArms';
import Hand from './hand/Hand';
import Arm1 from './arm1/Arm1';
import KaliArms from './KaliArms';
import VishnuArms from './VishnuArms';
import global from '../../../utilities/globalStuff';

const Arms = ({ name, isPointing, fill, stroke }) => (
  <g>
    {name === 'Shiva' && (
      <g>
        <ShivaArms fill={fill} stroke={stroke} />
        <Arm1 isPointing={isPointing} fill={fill} stroke={stroke} name={name} />
      </g>
    )}
    {name === 'Ganesha' && (
      <g>
        <GaneshaArms fill={fill} stroke={stroke} />
        <Arm1 isPointing={isPointing} fill={fill} stroke={stroke} name={name} />
      </g>
    )}
    {name === 'Parvati' && (
      <g>
        <ParvatiArms fill={fill} stroke={stroke} />
        <Hand name={name} isPointing={isPointing} fill={fill} stroke={stroke} />
      </g>
    )}
    {name === 'Lakshmi' && (
      <g>
        <LakshmiArms fill={fill} stroke={stroke} />
        <Hand name={name} isPointing={isPointing} fill={fill} stroke={stroke} />
      </g>
    )}
    {name === 'Kali' && (
      <g>
        <KaliArms fill={fill} stroke={stroke} />
        <Hand name={name} isPointing={isPointing} fill={fill} stroke={stroke} />
      </g>
    )}
    {name === 'Vishnu' && (
      <g>
        <VishnuArms fill={fill} stroke={stroke} />
        <Arm1 isPointing={isPointing} fill={fill} stroke={stroke} name={name} />
      </g>
    )}
  </g>
);

Arms.defaultProps = {
  name: 'Shiva',
  fill: '',
  stroke: '',
  isPointing: false
};

Arms.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  fill: PropTypes.string,
  stroke: PropTypes.string,
  isPointing: PropTypes.bool
};

export { Arms as default };
