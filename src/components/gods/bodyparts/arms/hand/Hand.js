import React from 'react';
import PropTypes from 'prop-types';
import HandPoint from './HandPoint';
import HandOpen from './HandOpen';
import LakshmiHandOpen from './LakshmiHandOpen';
import global from '../../../../utilities/globalStuff';

const Hand = ({ isPointing, fill, stroke, name }) => (
  <g id="hand">
    {isPointing ? (
      <HandPoint fill={fill} stroke={stroke} name={name} />
    ) : name === 'Lakshmi' ? (
      <LakshmiHandOpen fill={fill} stroke={stroke} />
    ) : (
      <HandOpen fill={fill} stroke={stroke} />
    )}
  </g>
);

Hand.defaultProps = {
  name: 'Shiva',
  isPointing: false,
  fill: '',
  stroke: ''
};

Hand.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  isPointing: PropTypes.bool,
  fill: PropTypes.string,
  stroke: PropTypes.string
};

export { Hand as default };
