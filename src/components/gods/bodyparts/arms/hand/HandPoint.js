import React from 'react';
import { TweenMax, TimelineMax } from 'gsap';
import PropTypes from 'prop-types';
import animate from '../../../../utilities/animations';
import global from '../../../../utilities/globalStuff';
import ParvatiHandPoint from './ParvatiHandPoint';
import LakshmiHandPoint from './LakshmiHandPoint';

class HandPoint extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      const tl1 = new TimelineMax();
      const tl2 = new TimelineMax();
      tl1.to('#pupil-right, #pupil-left', 0, { x: 0 });
      tl2.to('#eyes-default-closed', 0, { opacity: 1 });
      TweenMax.killTweensOf('#pupil-right, #pupil-left, #eyes-default-closed');
      animate.wagFingerHandOnly(this.props.name);
    }, 200);
  }
  componentWillUnmount() {
    TweenMax.killTweensOf('#arm1-point');
    animate.idleEyes();
  }
  render() {
    const { name, fill, stroke } = this.props;
    return (
      <g>
        {name === 'Lakshmi' ? (
          <LakshmiHandPoint fill={fill} stroke={stroke} />
        ) : (
          <ParvatiHandPoint fill={fill} stroke={stroke} />
        )}
      </g>
    );
  }
}

HandPoint.defaultProps = {
  name: 'Shiva',
  fill: '',
  stroke: ''
};

HandPoint.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  fill: PropTypes.string,
  stroke: PropTypes.string
};

export { HandPoint as default };
