import React from 'react';

const GaneshaPattern = () => (
  <pattern
    x="-188.612"
    y="-21.848"
    width="45"
    height="45"
    patternUnits="userSpaceOnUse"
    id="New_Pattern_Swatch_3"
    viewBox="0 -45 45 45"
    overflow="visible"
  >
    <path fill="none" d="M0-45h45V0H0z" />
    <path
      fill="#A80DCE"
      d="M14.596-22.355c14.376-21.657 26.305-12.145 0 0 26.545 11.617 14.81 21.366 0 0"
    />
    <path fill="none" d="M45 0H0v-45h45z" />
  </pattern>
);

export { GaneshaPattern as default };
