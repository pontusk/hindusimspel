import React from 'react';
import PropTypes from 'prop-types';
import { TweenMax } from 'gsap';
import animate from '../../../../../utilities/animations';
import global from '../../../../../utilities/globalStuff';
import MouthDefaultOpen from './MouthDefaultOpen';
import MouthGodessOpen from './MouthGodessOpen';
import MouthKaliOpen from './MouthKaliOpen';
import VishnuMouthOpen from './VishnuMouthOpen';

class MouthOpen extends React.Component {
  componentDidMount() {
    TweenMax.to('#pupil-right, #pupil-left', 0, { x: 0 });
    TweenMax.killTweensOf(document.getElementById('pupil-right'));
    TweenMax.killTweensOf(document.getElementById('pupil-left'));
    if (this.props.name === 'Kali') {
      TweenMax.to('#eyes-default-closed', 0, { opacity: 0 });
    } else {
      TweenMax.to('#eyes-default-closed', 0, { opacity: 1 });
    }
    TweenMax.killTweensOf(document.getElementById('eyes-default-closed'));
  }
  componentWillUnmount() {
    if (this.props.name === 'Kali') {
      TweenMax.to('#eyes-default-closed', 0, { opacity: 1 });
    }
    animate.idleEyes();
  }
  mouth() {
    let mouth;
    const { name } = this.props;

    if (name === 'Lakshmi' || name === 'Parvati') {
      mouth = <MouthGodessOpen />;
    } else if (name === 'Kali') {
      mouth = <MouthKaliOpen />;
    } else if (name === 'Vishnu') {
      mouth = <VishnuMouthOpen />;
    } else {
      mouth = <MouthDefaultOpen />;
    }
    return mouth;
  }
  render() {
    return this.mouth();
  }
}

MouthOpen.defaultProps = {
  name: 'Shiva',
  isSmiling: false,
  isPointing: false
};

MouthOpen.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  isSmiling: PropTypes.bool,
  isPointing: PropTypes.bool
};

export { MouthOpen as default };
