import React from 'react';
import PropTypes from 'prop-types';

import MouthOpen from './mouthopen/MouthOpen';
import MouthDefaultClosed from './MouthDefaultClosed';
import MouthGodessClosed from './MouthGodessClosed';
import KaliMouthFrown from './MouthKaliFrown';
import KaliMouthClosed from './MouthKaliClosed';
import VishnuMouthClosed from './VishnuMouthClosed';
import global from '../../../../utilities/globalStuff';

class Mouth extends React.Component {
  mouth() {
    // renders different mouth depending on god
    let mouth;
    const { name } = this.props;

    if (name === 'Parvati' || name === 'Lakshmi') {
      if (this.props.isSmiling) {
        mouth = <MouthOpen name={name} />;
      } else {
        mouth = <MouthGodessClosed />;
      }
    } else if (this.props.name === 'Shiva') {
      if (this.props.isSmiling) {
        mouth = <MouthOpen name={name} />;
      } else {
        mouth = <MouthDefaultClosed />;
      }
    } else if (this.props.name === 'Kali') {
      if (this.props.isSmiling) {
        mouth = <MouthOpen name={name} />;
      } else if (this.props.isPointing) {
        mouth = <KaliMouthFrown />;
      } else {
        mouth = <KaliMouthClosed />;
      }
    } else if (this.props.name === 'Vishnu') {
      if (this.props.isSmiling) {
        mouth = <MouthOpen name={name} />;
      } else {
        mouth = <VishnuMouthClosed />;
      }
    }
    return mouth;
  }
  render() {
    return <g id="mouth-default">{this.mouth()}</g>;
  }
}

Mouth.defaultProps = {
  name: 'Shiva',
  isSmiling: false,
  isPointing: false
};

Mouth.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  isSmiling: PropTypes.bool,
  isPointing: PropTypes.bool
};

export { Mouth as default };
