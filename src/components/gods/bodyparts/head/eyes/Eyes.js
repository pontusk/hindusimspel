import React from 'react';
import PropTypes from 'prop-types';
import color from '../../../../utilities/colors';
import global from '../../../../utilities/globalStuff';
import EyesDefaultOpen from './EyesDefaultOpen';
import EyesClosed from './eyesclosed/EyesClosed';
import GaneshaEyesOpen from './GaneshaEyesOpen';
import EyesGodessOpen from './EyesGodessOpen';
import GodessEyebrows from './GodessEyebrows';
import LakshmiEyebrows from './LakshmiEyebrows';
import KaliEyesOpen from './KaliEyesOpen';

class Eyes extends React.Component {
  eyes() {
    // Renders different eyes depending on the god and depending on state
    const { name, isPointing, isWinking } = this.props;
    let eyes;
    if (isWinking || isPointing) {
      if (name === 'Shiva' || name === 'Vishnu') {
        eyes = (
          <g>
            <EyesDefaultOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
            <EyesClosed
              name={name}
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      } else if (name === 'Ganesha') {
        eyes = (
          <g>
            <GaneshaEyesOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
            <EyesClosed
              name={name}
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      } else if (name === 'Parvati') {
        eyes = (
          <g>
            <GodessEyebrows />
            <EyesGodessOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
            <EyesClosed
              name={name}
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      } else if (name === 'Lakshmi') {
        eyes = (
          <g>
            <LakshmiEyebrows />
            <EyesGodessOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
            <EyesClosed
              name={name}
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      } else if (name === 'Kali') {
        eyes = (
          <g>
            <KaliEyesOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
            <EyesClosed
              name={name}
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      }
    } else if (!isWinking || !isPointing) {
      if (name === 'Shiva' || name === 'Vishnu') {
        eyes = (
          <EyesDefaultOpen
            fill={color.skinColor(name)[0]}
            eyeFill={color.skinColor(name)[3]}
            eyeStroke={color.skinColor(name)[4]}
          />
        );
      } else if (name === 'Ganesha') {
        eyes = (
          <GaneshaEyesOpen
            fill={color.skinColor(name)[0]}
            eyeFill={color.skinColor(name)[3]}
            eyeStroke={color.skinColor(name)[4]}
          />
        );
      } else if (name === 'Parvati') {
        eyes = (
          <g>
            <GodessEyebrows />
            <EyesGodessOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      } else if (name === 'Lakshmi') {
        eyes = (
          <g>
            <LakshmiEyebrows />
            <EyesGodessOpen
              fill={color.skinColor(name)[0]}
              eyeFill={color.skinColor(name)[3]}
              eyeStroke={color.skinColor(name)[4]}
            />
          </g>
        );
      } else if (name === 'Kali') {
        eyes = (
          <KaliEyesOpen
            fill={color.skinColor(name)[0]}
            eyeFill={color.skinColor(name)[3]}
            eyeStroke={color.skinColor(name)[4]}
          />
        );
      }
    }
    return eyes;
  }
  render() {
    return <g id="eyes-default">{this.eyes()}</g>;
  }
}

Eyes.defaultProps = {
  name: 'Shiva',
  isWinking: false,
  isPointing: false
};

Eyes.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  isWinking: PropTypes.bool,
  isPointing: PropTypes.bool
};

export { Eyes as default };
