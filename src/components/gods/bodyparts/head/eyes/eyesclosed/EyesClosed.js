import React from 'react';
import PropTypes from 'prop-types';
import { TweenMax } from 'gsap';
import animate from '../../../../../utilities/animations';
import global from '../../../../../utilities/globalStuff';
import EyesGodessClosed from './EyesGodessClosed';
import KaliEyesClosed from './KaliEyesClosed';
import EyesDefaultClosed from './EyesDefaultClosed';
import GaneshaEyesClosed from './GaneshaEyesClosed';

class EyesClosed extends React.Component {
  componentDidMount() {
    animate.idleEyes();
  }
  componentWillUnmount() {
    TweenMax.to('#pupil-right, #pupil-left', 0, { x: 0 });
    TweenMax.to('#eyes-default-closed', 0, { opacity: 1 });
    TweenMax.killTweensOf('#pupil-right, #pupil-left, #eyes-default-closed');
  }
  eyes() {
    // Renders different closed eyes depending on god
    let eyes;
    const { name, eyeFill, eyeStroke } = this.props;
    if (name === 'Lakshmi' || name === 'Parvati') {
      eyes = <EyesGodessClosed eyeFill={eyeFill} eyeStroke={eyeStroke} />;
    } else if (name === 'Kali') {
      eyes = <KaliEyesClosed eyeFill={eyeFill} eyeStroke={eyeStroke} />;
    } else if (name === 'Ganesha') {
      eyes = <GaneshaEyesClosed eyeFill={eyeFill} eyeStroke={eyeStroke} />;
    } else {
      eyes = <EyesDefaultClosed eyeFill={eyeFill} eyeStroke={eyeStroke} />;
    }
    return eyes;
  }
  render() {
    return this.eyes();
  }
}

EyesClosed.defaultProps = {
  name: 'Shiva',
  eyeFill: '',
  eyeStroke: ''
};

EyesClosed.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  eyeFill: PropTypes.string,
  eyeStroke: PropTypes.string
};

export { EyesClosed as default };
