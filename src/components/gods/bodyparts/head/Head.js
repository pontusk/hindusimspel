import React from 'react';
import PropTypes from 'prop-types';

import ShivaHeadBase from './ShivaHeadBase';
import GaneshaHeadBase from './GaneshaHeadBase';
import ParvatiHeadBase from './ParvatiHeadBase';
import LakshmiHeadBase from './LakshmiHeadBase';
import Snout from './Snout';
import NoseDefault from './NoseDefault';
import NoseGodess from './NoseGodess';
import LakshmiNose from './LakshmiNose';
import Crown from './Crown';
import LakshmiCrown from './LakshmiCrown';
import Mouth from './mouth/Mouth';
import Eyes from './eyes/Eyes';
import KaliHeadBase from './KaliHeadBase';
import KaliNose from './KaliNose';
import KaliCrown from './KaliCrown';
import VishnuHeadBase from './VishnuHeadBase';
import VishnuNose from './VishnuNose';
import VishnuCrown from './VishnuCrown';
import global from '../../../utilities/globalStuff';

const Head = ({
  name,
  isSmiling,
  isWinking,
  isPointing,
  fill,
  stroke,
  noseStroke,
  eyeFill,
  noseFill
}) => (
  <g id="head">
    {name === 'Shiva' && (
      <g>
        <ShivaHeadBase fill={fill} stroke={stroke} />
        <NoseDefault noseStroke={noseStroke} noseFill={noseFill} />
      </g>
    )}
    {name === 'Ganesha' && (
      <g>
        <GaneshaHeadBase fill={fill} stroke={stroke} />
        <Snout fill={fill} stroke={stroke} isSmiling={isSmiling} />
      </g>
    )}
    {name === 'Parvati' && (
      <g>
        <ParvatiHeadBase fill={fill} stroke={stroke} />
        <NoseGodess noseStroke={noseStroke} eyeFill={eyeFill} />
        <Crown />
      </g>
    )}
    {name === 'Lakshmi' && (
      <g>
        <LakshmiHeadBase fill={fill} stroke={stroke} />
        <LakshmiNose fill={fill} stroke={stroke} />
        <LakshmiCrown />
      </g>
    )}
    {name === 'Kali' && (
      <g>
        <KaliHeadBase fill={fill} stroke={stroke} />
        <KaliNose noseStroke={noseStroke} noseFill={noseFill} />
        <KaliCrown />
      </g>
    )}
    {name === 'Vishnu' && (
      <g>
        <VishnuHeadBase fill={fill} stroke={stroke} />
        <VishnuNose noseStroke={noseStroke} noseFill={noseFill} />
        <VishnuCrown />
      </g>
    )}
    {name !== 'Ganesha' && <Mouth isSmiling={isSmiling} isPointing={isPointing} name={name} />}
    <Eyes name={name} isWinking={isWinking} isPointing={isPointing} />
  </g>
);

Head.defaultProps = {
  name: 'Shiva',
  isSmiling: false,
  isWinking: false,
  isPointing: false,
  fill: '',
  stroke: '',
  noseStroke: '',
  eyeFill: '',
  noseFill: ''
};

Head.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  isSmiling: PropTypes.bool,
  isWinking: PropTypes.bool,
  isPointing: PropTypes.bool,
  fill: PropTypes.string,
  stroke: PropTypes.string,
  noseStroke: PropTypes.string,
  eyeFill: PropTypes.string,
  noseFill: PropTypes.string
};

export { Head as default };
