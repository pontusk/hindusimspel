import React from 'react';
import PropTypes from 'prop-types';

const LegsLean = ({ fill, stroke }) => (
  <g strokeWidth="3" strokeMiterlimit="10" fill={fill} stroke={stroke} id="legs">
    <path d="M670.545 709.335c-23.095-26.207-176.126-77.683-215.208-77.683h-128.2c-39.083 0-192.114 50.83-215.21 77.036-22.97 26.066-13.794 54.478 5.168 65.025l-.175-.104c27.05 17.126 73.207 22.427 128.128 19.714 40.567-2.004 93.455-14.656 144.464-28.506 51.01 13.85 103.897 27.055 144.464 29.06 53.83 2.658 99.227-1.226 126.48-18.226h2.648c20.47-9 31.315-39.226 7.44-66.317z" />
    <path d="M170.416 728.787s96.22 13.698 142.2 6.75l129.71 14.457c-64.848 18.38-142.357 40.492-197.278 43.205s-101.08-2.714-128.128-19.84M466.407 735.537c45.98 6.948 138.817-12.31 138.817-12.31" />
  </g>
);

LegsLean.defaultProps = {
  fill: '',
  stroke: ''
};

LegsLean.propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string
};

export { LegsLean as default };
