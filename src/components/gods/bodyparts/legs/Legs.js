import React from 'react';
import PropTypes from 'prop-types';
import LegsLean from './LegsLean';
import GaneshaLegs from './GaneshaLegs';
import ParvatiLegs from './ParvatiLegs';
import LakshmiLegs from './LakshmiLegs';
import KaliLegs from './KaliLegs';
import VisnhuLegs from './VishnuLegs';
import global from '../../../utilities/globalStuff';

const Legs = ({ name, fill, stroke }) => (
  <g>
    {name === 'Shiva' && <LegsLean fill={fill} stroke={stroke} />}
    {name === 'Ganesha' && <GaneshaLegs fill={fill} stroke={stroke} />}
    {name === 'Parvati' && <ParvatiLegs fill={fill} stroke={stroke} />}
    {name === 'Lakshmi' && <LakshmiLegs fill={fill} stroke={stroke} />}
    {name === 'Kali' && <KaliLegs fill={fill} stroke={stroke} />}
    {name === 'Vishnu' && <VisnhuLegs fill={fill} stroke={stroke} />}
  </g>
);

Legs.defaultProps = {
  name: 'Shiva',
  fill: '',
  stroke: ''
};

Legs.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  fill: PropTypes.string,
  stroke: PropTypes.string
};

export { Legs as default };
