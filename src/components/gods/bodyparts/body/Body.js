import React from 'react';
import PropTypes from 'prop-types';
import BodyLean from './BodyLean';
import ShivaClothes from './ShivaClothes';
import GaneshaBody from './GaneshaBody';
import Patch from './Patch';
import ParvatiBody from './ParvatiBody';
import LakshmiBody from './LakshmiBody';
import KaliBody from './KaliBody';
import KaliBones from './KaliBones';
import KaliPatch from './KaliPatch';
import VishnuClothes from './VishnuClothes';
import global from '../../../utilities/globalStuff';

const Body = ({ name, fill, stroke, hasAttr6 }) => (
  <g>
    {name === 'Shiva' && (
      <g>
        <BodyLean fill={fill} stroke={stroke} />
        <ShivaClothes />
        <Patch fill={fill} stroke={stroke} />
      </g>
    )}
    {name === 'Ganesha' && <GaneshaBody fill={fill} stroke={stroke} />}
    {name === 'Parvati' && <ParvatiBody />}
    {name === 'Lakshmi' && <LakshmiBody />}
    {name === 'Kali' && (
      <g>
        <KaliBody fill={fill} stroke={stroke} />
        {hasAttr6 && <KaliBones />}
        <KaliPatch fill={fill} stroke={stroke} />
      </g>
    )}
    {name === 'Vishnu' && (
      <g>
        <BodyLean fill={fill} stroke={stroke} />
        <VishnuClothes />
      </g>
    )}
  </g>
);

Body.defaultProps = {
  name: 'Shiva',
  fill: '',
  stroke: '',
  hasAttr6: false
};

Body.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  fill: PropTypes.string,
  stroke: PropTypes.string,
  hasAttr6: PropTypes.bool
};

export { Body as default };
