import React from 'react';
import PropTypes from 'prop-types';
import God from './gods/God';
import GodSounds from './gods/GodSounds';
import Drawer from './drawer/Drawer';
import Navigation from './navigation/Navigation';
import OnFinishedGod from './OnFinishedGod';
import global from './utilities/globalStuff';
import InfoAbout from './navigation/InfoAbout';

class Main extends React.Component {
  constructor(props) {
    super(props);

    // all the local state
    this.state = {
      isPointing: false,
      isSmiling: false,
      isWinking: true,
      hasAttr1: false, // Toggles attribute specific to the God in question
      hasAttr2: false,
      hasAttr3: false,
      hasAttr4: false,
      hasAttr5: false,
      hasAttr6: false,
      hasAttr7: false,
      hasAttr8: false,
      hasAttr9: false,
      hasAttr10: false,
      showInfo: 'closed'
    };

    this.updateState = this.updateState.bind(this);
  }

  updateState(newState) {
    this.setState(newState);
  }
  finished() {
    // determens what is needed to finish different gods
    let finished;
    const {
      hasAttr1,
      hasAttr2,
      hasAttr3,
      hasAttr4,
      hasAttr5,
      hasAttr6,
      hasAttr7,
      hasAttr8,
      hasAttr9
    } = this.state;
    const { name, addGod, finishedGods, switchSound, soundPlaying } = this.props;

    if (name === 'Lakshmi') {
      // Needs 2 attributes to finish
      if (hasAttr1 && hasAttr2) {
        finished = (
          <OnFinishedGod
            name={name}
            addGod={addGod}
            soundPlaying={soundPlaying}
            switchSound={switchSound}
            finishedGods={finishedGods}
          />
        );
      } else {
        finished = undefined;
      }
    } else if (name === 'Shiva') {
      // Needs 3 attributes to finish
      if (hasAttr1 && hasAttr2 && hasAttr3) {
        finished = (
          <OnFinishedGod
            name={name}
            addGod={addGod}
            soundPlaying={soundPlaying}
            switchSound={switchSound}
            finishedGods={finishedGods}
          />
        );
      } else {
        finished = undefined;
      }
    } else if (name === 'Vishnu' || name === 'Ganesha') {
      // Needs 4 attributes to finish
      if (hasAttr1 && hasAttr2 && hasAttr3 && hasAttr4) {
        finished = (
          <OnFinishedGod
            name={name}
            addGod={addGod}
            soundPlaying={soundPlaying}
            switchSound={switchSound}
            finishedGods={finishedGods}
          />
        );
      } else {
        finished = undefined;
      }
    } else if (name === 'Kali') {
      // Needs 8 attributes to finish
      if (
        hasAttr1 &&
        hasAttr2 &&
        hasAttr3 &&
        hasAttr4 &&
        hasAttr5 &&
        hasAttr6 &&
        hasAttr7 &&
        hasAttr8
      ) {
        finished = (
          <OnFinishedGod
            name={name}
            addGod={addGod}
            soundPlaying={soundPlaying}
            switchSound={switchSound}
            finishedGods={finishedGods}
          />
        );
      } else {
        finished = undefined;
      }
    } else if (name === 'Parvati') {
      // Needs 9 attributes to finish
      if (
        hasAttr1 &&
        hasAttr2 &&
        hasAttr3 &&
        hasAttr4 &&
        hasAttr5 &&
        hasAttr6 &&
        hasAttr7 &&
        hasAttr8 &&
        hasAttr9
      ) {
        finished = (
          <OnFinishedGod
            name={name}
            soundPlaying={soundPlaying}
            switchSound={switchSound}
            addGod={addGod}
            finishedGods={finishedGods}
          />
        );
      } else {
        finished = undefined;
      }
    }
    return finished;
  }
  render() {
    const {
      isPointing,
      isSmiling,
      isWinking,
      hasAttr1,
      hasAttr2,
      hasAttr3,
      hasAttr4,
      hasAttr5,
      hasAttr6,
      hasAttr7,
      hasAttr8,
      hasAttr9,
      hasAttr10,
      showInfo
    } = this.state;
    const {
      name,
      finishedGods,
      incrementAttempts,
      attemptsLog,
      soundPlaying,
      stopSound,
      playSound,
      currentSound,
      switchSound
    } = this.props;
    return (
      <main className="main-main">
        <section className="character-container">
          <div className="character">
            <div
              className={
                // different placement for hill for the godesses
                name === 'Parvati' || name === 'Lakshmi' || name === 'Kali'
                  ? 'godess-hill-style'
                  : 'hill-style'
              }
            />
            <God
              name={name}
              styled
              isPointing={isPointing}
              isSmiling={isSmiling}
              isWinking={isWinking}
              hasAttr1={hasAttr1}
              hasAttr2={hasAttr2}
              hasAttr3={hasAttr3}
              hasAttr4={hasAttr4}
              hasAttr5={hasAttr5}
              hasAttr6={hasAttr6}
              hasAttr7={hasAttr7}
              hasAttr8={hasAttr8}
              hasAttr9={hasAttr9}
              hasAttr10={hasAttr10}
              showInfo={showInfo}
              updateState={this.updateState}
              finishedGods={finishedGods}
              soundPlaying={soundPlaying}
            />
            <GodSounds
              name={name}
              playSound={playSound}
              stopSound={stopSound}
              soundPlaying={soundPlaying}
              currentSound={currentSound}
              switchSound={switchSound}
            />
          </div>
          <Navigation
            name={name}
            updateState={this.updateState}
            showInfo={showInfo}
            playSound={playSound}
            stopSound={stopSound}
            soundPlaying={soundPlaying}
            currentSound={currentSound}
            switchSound={switchSound}
          />
        </section>
        <Drawer
          name={name}
          updateState={this.updateState}
          switchSound={switchSound}
          incrementAttempts={incrementAttempts}
          attemptsLog={attemptsLog}
        />
        {showInfo !== 'closed' && (
          <InfoAbout
            showInfo={showInfo}
            name={name}
            finishedGods={finishedGods}
            updateState={this.updateState}
            switchSound={switchSound}
          />
        )}
        {this.finished()}
      </main>
    );
  }
}

Main.defaultProps = {
  currentSound: 'none',
  switchSound: () => console.log('prop is missing'),
  playSound: () => console.log('prop is missing'),
  stopSound: () => console.log('prop is missing'),
  soundPlaying: true,
  name: 'Shiva',
  addGod: () => console.log('prop is missing'),
  finishedGods: [],
  children: undefined,
  incrementAttempts: () => console.log('prop is missing'),
  attemptsLog: 0
};

Main.propTypes = {
  currentSound: PropTypes.string,
  switchSound: PropTypes.func,
  playSound: PropTypes.func,
  stopSound: PropTypes.func,
  soundPlaying: PropTypes.bool,
  name: PropTypes.oneOf(global.gods()),
  addGod: PropTypes.func,
  finishedGods: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.node,
  incrementAttempts: PropTypes.func,
  attemptsLog: PropTypes.number
};

export { Main as default };
