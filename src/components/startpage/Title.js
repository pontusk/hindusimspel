import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/actionCreators';
import TitlePresentation from './TitlePresentation';

function mapStateToProps(state) {
  return {
    finishedGods: state.finishedGods,
    attemptsLog: state.attemptsLog,
    soundPlaying: state.soundPlaying,
    currentSound: state.currentSound
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}
const Title = connect(mapStateToProps, mapDispatchToProps)(TitlePresentation);

export { Title as default };
