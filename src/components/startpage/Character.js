import React from 'react';
import PropTypes from 'prop-types';
import God from '../gods/God';
import global from '../utilities/globalStuff';

const Character = ({ name }) => (
  // if the god is finished, the icon will be grayed out and the link disabled
  // the finishedGods array comes from Redux
  <article alt={name} className={`${name.toLowerCase()}-article`}>
    <div>
      <div className="portrait">
        <God name={name} />
      </div>
    </div>
  </article>
);

Character.defaultProps = {
  switchSound: () => console.log('prop is missing'),
  name: 'Shiva'
};

Character.propTypes = {
  switchSound: PropTypes.func,
  name: PropTypes.oneOf(global.gods())
};

export { Character as default };
