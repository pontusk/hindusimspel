import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../../actions/actionCreators';
import StartPresentation from './StartPresentation';

function mapStateToProps(state) {
  return {
    finishedGods: state.finishedGods,
    attemptsLog: state.attemptsLog,
    soundPlaying: state.soundPlaying,
    currentSound: state.currentSound
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}
const Start = connect(mapStateToProps, mapDispatchToProps)(StartPresentation);

export { Start as default };
