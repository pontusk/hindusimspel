import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import Logo from '../general/Logo';
import Button from '../general/Button';
import TextBox from '../general/TextBox';
import TitleText from '../texts/TitleText';
import Navigation from '../navigation/Navigation';

class Title extends React.Component {
  static maxHeight() {
    let maxHeight;
    if (window.matchMedia('(max-width: 768px)').matches) {
      maxHeight = `calc(${window.innerHeight} * 0.60px - 5vw - 70px - 3em)`;
    } else {
      maxHeight = 'calc(60vh - 5vw - 70px - 3em)';
    }
    return maxHeight;
  }
  componentDidMount() {
    let h;
    if (window.matchMedia('(max-width: 768px)').matches) {
      h = `calc(${window.innerHeight} * 0.60px - 5vw - 70px - 3em)`;
      document.querySelector('.info-box article').style.height = h;
    }
  }
  handleClick() {
    this.props.router.push('start');
    this.props.switchSound('click');
  }
  render() {
    const { soundPlaying, playSound, stopSound, switchSound } = this.props;

    return (
      <div className="title-page">
        <div className="title-container">
          <Navigation
            isTitle
            soundPlaying={soundPlaying}
            playSound={playSound}
            stopSound={stopSound}
            switchSound={switchSound}
          />
          <div className="logo-container">
            <Logo />
          </div>
          <div className="infobox-container">
            <div className="info-box">
              <TextBox maxHeight={Title.maxHeight}>
                <TitleText />
              </TextBox>
              <div className="button-container">
                <Button title="Spela" isInline onClick={() => this.handleClick()} />
              </div>
              {/* button-container */}
            </div>
            {/* info-box */}
          </div>
          {/* infobox-container */}
        </div>
        {/* title-container */}
      </div>
      // title-page
    );
  }
}

Title.defaultProps = {
  switchSound: () => console.log('prop is missing'),
  currentSound: 'none',
  playSound: () => console.log('prop is missing'),
  stopSound: () => console.log('prop is missing'),
  soundPlaying: true
};

Title.propTypes = {
  switchSound: PropTypes.func,
  currentSound: PropTypes.string,
  playSound: PropTypes.func,
  stopSound: PropTypes.func,
  soundPlaying: PropTypes.bool,
  router: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

const routedTitle = withRouter(Title);

export { routedTitle as default };
