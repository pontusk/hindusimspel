import React from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import global from '../utilities/globalStuff';

const Medal = ({ name, finishedGods, switchSound }) => (
  <article alt={name} title={name} className={`${name.toLowerCase()}-medal medal-container`}>
    <Link to={`/${name}`} onClick={() => switchSound('click')}>
      {finishedGods.indexOf(name) > -1 && (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 216.857 216.904" id="medal">
          <g fill="#FFF">
            <path d="M195.174 125.756l21.683-17.264-21.683-17.264 13.453-24.237-26.655-7.615 3.132-27.543-27.542 3.133-7.617-26.656-24.237 13.453L108.444.08 91.18 21.762 66.945 8.309l-7.616 26.656-27.543-3.133 3.132 27.543-26.656 7.616 13.453 24.237L.032 108.492l21.683 17.264-13.454 24.236 26.656 7.616-3.133 27.543 27.543-3.132 7.615 26.656 24.237-13.453 17.264 21.683 17.264-21.683 24.236 13.453 7.615-26.656 27.543 3.133-3.132-27.543 26.656-7.616-13.451-24.237zm-86.73 55.752c-40.326 0-73.016-32.69-73.016-73.016s32.69-73.016 73.016-73.016 73.016 32.69 73.016 73.016-32.69 73.016-73.016 73.016z" />
            <circle cx="108.444" cy="108.492" r="64.821" />
          </g>
        </svg>
      )}
    </Link>
  </article>
);

Medal.defaultProps = {
  switchSound: () => console.log('prop is missing'),
  name: 'Shiva',
  finishedGods: []
};

Medal.propTypes = {
  switchSound: PropTypes.func,
  name: PropTypes.oneOf(global.gods()),
  finishedGods: PropTypes.arrayOf(PropTypes.string)
};

export { Medal as default };
