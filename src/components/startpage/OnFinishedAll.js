import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import Sound from 'react-sound';
import Button from '../general/Button';
import TextBox from '../general/TextBox';
import SuccessText from '../texts/SuccessText';
import cheer from '../../sounds/cheer.mp3';

class OnFinishedAll extends React.Component {
  static maxHeight() {
    let maxHeight;
    if (window.matchMedia('(max-width: 768px)').matches) {
      maxHeight = `calc(${window.innerHeight}px - 70px - 10vw - 3em)`;
    } else {
      maxHeight = 'calc(74vh - 50px - 1em)';
    }
    return maxHeight;
  }
  constructor(props) {
    super(props);
    this.state = {
      cheerPlaying: true
    };
    this.handleFinishedPlaying = this.handleFinishedPlaying.bind(this);
  }
  componentDidMount() {
    let h;
    if (window.matchMedia('(max-width: 768px)').matches) {
      h = `calc(${window.innerHeight}px - 70px - 10vw - 3em)`;
      document.querySelector('.info-box article').style.height = h;
    }
  }
  handleClick() {
    this.props.switchSound('click');
    this.props.clearGods();
    this.props.clearAttempts();
  }
  cheerPlay() {
    let status = Sound.status.PLAYING;
    if (!this.state.cheerPlaying) {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  handleFinishedPlaying() {
    this.setState({ cheerPlaying: false });
  }
  render() {
    const { attemptsLog, soundPlaying } = this.props;
    return (
      <div className="success-container">
        <div className="on-finished-all info-box">
          <TextBox maxHeight={OnFinishedAll.maxHeight}>
            <SuccessText attemptsLog={attemptsLog} />
          </TextBox>
          <div className="button-container">
            <Button onClick={() => this.handleClick()} title="Spela igen" />
          </div>
        </div>
        {soundPlaying && (
          <Sound
            url={cheer}
            autoPlay
            playStatus={this.cheerPlay()}
            onFinishedPlaying={this.handleFinishedPlaying}
          />
        )}
      </div>
    );
  }
}

OnFinishedAll.propTypes = {
  router: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

OnFinishedAll.defaultProps = {
  soundPlaying: false,
  switchSound: () => console.log('prop is missing'),
  clearGods: () => console.log('prop is missing'),
  clearAttempts: () => console.log('prop is missing'),
  attemptsLog: 0
};

OnFinishedAll.propTypes = {
  soundPlaying: PropTypes.bool,
  switchSound: PropTypes.func,
  clearGods: PropTypes.func,
  clearAttempts: PropTypes.func,
  attemptsLog: PropTypes.number
};

const routedOnFinishedAll = withRouter(OnFinishedAll);

export { routedOnFinishedAll as default };
