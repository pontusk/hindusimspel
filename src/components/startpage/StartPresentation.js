import React from 'react';
import PropTypes from 'prop-types';
import Character from './Character';
import OnFinishedAll from './OnFinishedAll';
import Medal from './Medal';
import global from '../utilities/globalStuff';

class Start extends React.Component {
  componentDidMount() {
    console.log(JSON.stringify(this.props.finishedGods.sort()));
  }
  finishedAll() {
    let finishedAll;
    const {
      finishedGods,
      clearGods,
      clearAttempts,
      attemptsLog,
      switchSound,
      soundPlaying
    } = this.props;
    if (
      finishedGods.indexOf('Ganesha') > -1 &&
      finishedGods.indexOf('Kali') > -1 &&
      finishedGods.indexOf('Lakshmi') > -1 &&
      finishedGods.indexOf('Parvati') > -1 &&
      finishedGods.indexOf('Shiva') > -1 &&
      finishedGods.indexOf('Vishnu') > -1
    ) {
      finishedAll = (
        <OnFinishedAll
          clearGods={clearGods}
          clearAttempts={clearAttempts}
          attemptsLog={attemptsLog}
          switchSound={switchSound}
          soundPlaying={soundPlaying}
        />
      );
    }
    return finishedAll;
  }
  renderGods() {
    return this.props.gods.map(god => (
      <Character switchSound={this.props.switchSound} name={god} key={god.toString()} />
    ));
  }
  renderMedal() {
    return this.props.gods.map(god => (
      <Medal
        switchSound={this.props.switchSound}
        name={god}
        finishedGods={this.props.finishedGods}
        key={`${god.toString()}-medal`}
      />
    ));
  }
  render() {
    return (
      <main className="start-page">
        <div className="start-container">
          <section className="character-section">{this.renderGods()}</section>
          <section className="medal-section">{this.renderMedal()}</section>
        </div>
        {this.finishedAll()}
      </main>
    );
  }
}

Start.defaultProps = {
  soundPlaying: false,
  switchSound: () => console.log('prop is missing'),
  gods: global.gods(),
  finishedGods: [],
  clearGods: () => console.log('prop is missing'),
  clearAttempts: () => console.log('prop is missing'),
  attemptsLog: 0
};

Start.propTypes = {
  soundPlaying: PropTypes.bool,
  switchSound: PropTypes.func,
  gods: PropTypes.arrayOf(PropTypes.string),
  finishedGods: PropTypes.arrayOf(PropTypes.string),
  clearGods: PropTypes.func,
  clearAttempts: PropTypes.func,
  attemptsLog: PropTypes.number
};

export { Start as default };
