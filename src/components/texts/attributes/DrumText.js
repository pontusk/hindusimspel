import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class DrumText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Trumman</strong> symboliserar skapande.
          </p>
        </div>
      </div>
    );
  }
}

DrumText.defaultProps = {
  showInfo: 'closed'
};

DrumText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { DrumText as default };
