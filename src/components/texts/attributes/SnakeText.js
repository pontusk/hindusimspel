import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class SnakeText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Ormen</strong> symboliserar återfödelse och makt.
          </p>
        </div>
      </div>
    );
  }
}

SnakeText.defaultProps = {
  showInfo: 'closed'
};

SnakeText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { SnakeText as default };
