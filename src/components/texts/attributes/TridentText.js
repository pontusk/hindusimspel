import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class TridentText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Treudden</strong> symboliserar förstörelse.
          </p>
        </div>
      </div>
    );
  }
}

TridentText.defaultProps = {
  showInfo: 'closed'
};

TridentText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { TridentText as default };
