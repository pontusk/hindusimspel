import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class LotusText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Lotusen</strong> symboliserar renhet och utveckling.
          </p>
        </div>
      </div>
    );
  }
}

LotusText.defaultProps = {
  showInfo: 'closed'
};

LotusText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { LotusText as default };
