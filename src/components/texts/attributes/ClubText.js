import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class ClubText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Klubban</strong> symboliserar auktoritet.
          </p>
        </div>
      </div>
    );
  }
}

ClubText.defaultProps = {
  showInfo: 'closed'
};

ClubText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { ClubText as default };
