import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class ChakraText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Den gudomliga diskusen</strong> är en mäktig kraft i ständig rörelse. Den
            symboliserar tidens gång.
          </p>
        </div>
      </div>
    );
  }
}

ChakraText.defaultProps = {
  showInfo: 'closed'
};

ChakraText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { ChakraText as default };
