import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class BowlText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Godiset</strong> symboliserar belöningarna för ett gott liv.
          </p>
        </div>
      </div>
    );
  }
}

BowlText.defaultProps = {
  showInfo: 'closed'
};

BowlText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { BowlText as default };
