import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class RopeText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Repet</strong> symboliserar förmågan att fånga och uppnå sina mål.
          </p>
        </div>
      </div>
    );
  }
}

RopeText.defaultProps = {
  showInfo: 'closed'
};

RopeText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { RopeText as default };
