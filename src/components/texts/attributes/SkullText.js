import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class SkullText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Halsbandet av dödskallar</strong> symboliserar närvaro i alla människors
            medvetande.
          </p>
        </div>
      </div>
    );
  }
}

SkullText.defaultProps = {
  showInfo: 'closed'
};

SkullText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { SkullText as default };
