import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class AxeText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Yxan</strong> symboliserar förmågan att avlägsna hinder.
          </p>
        </div>
      </div>
    );
  }
}

AxeText.defaultProps = {
  showInfo: 'closed'
};

AxeText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { AxeText as default };
