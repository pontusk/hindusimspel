import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class MoneyText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Pengarna</strong> symboliserar lycka och överflöd.
          </p>
        </div>
      </div>
    );
  }
}

MoneyText.defaultProps = {
  showInfo: 'closed'
};

MoneyText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { MoneyText as default };
