import React from 'react';
import PropTypes from 'prop-types';
import Attribute from '../../drawer/Attribute';
import global from '../../utilities/globalStuff';

class ShellText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    const { showInfo } = this.props;
    return (
      <div>
        <div className="attr-pic-container">
          <Attribute className={`attr-${showInfo} item`} />
        </div>
        <div>
          <p className="last-para">
            <strong>Trumpetsnäckan</strong> innehåller kraften från elementen: vatten, eld, jord och
            vind. Den symboliserar renhet och energi.
          </p>
        </div>
      </div>
    );
  }
}

ShellText.defaultProps = {
  showInfo: 'closed'
};

ShellText.propTypes = {
  showInfo: PropTypes.oneOf(global.infoTypes())
};

export { ShellText as default };
