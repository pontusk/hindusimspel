import React from 'react';
import PropTypes from 'prop-types';
import global from '../utilities/globalStuff';
import color from '../utilities/colors';
import DragSymbol from './illustrations/DragSymbol';
import ClickSymbol from './illustrations/ClickSymbol';

class TutorialText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  pronoun() {
    const { name } = this.props;
    let pronoun;
    if (name === 'Shiva' || name === 'Vishnu' || name === 'Ganesha') {
      pronoun = 'honom';
    } else {
      pronoun = 'henne';
    }
    return pronoun;
  }
  render() {
    const { name } = this.props;
    return (
      <div>
        <p>
          Hinduismens gudar avbildas oftast med olika ägodelar. Sakerna har symbolisk betydelse och
          speglar gudarnas karaktär.
        </p>
        <DragSymbol fill={color.illustrationColor('orange')[0]} />
        <p>
          Ge {name} sakerna som du tror hör till {this.pronoun()} genom att dra och släppa.
        </p>
        <ClickSymbol fill={color.illustrationColor('orange')[0]} />
        <p className="last-para">Du kan klicka på sakerna för att få tips om vad de betyder.</p>
      </div>
    );
  }
}

TutorialText.defaultProps = {
  scrolled: false,
  name: 'Shiva'
};

TutorialText.propTypes = {
  scrolled: PropTypes.bool,
  name: PropTypes.oneOf(global.gods())
};

export { TutorialText as default };
