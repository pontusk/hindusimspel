import React from 'react';
import PropTypes from 'prop-types';
import global from '../utilities/globalStuff';
import ContactText from './contact/ContactText';

class TitleText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          Hinduismen är världens tredje största religion. Det finns över en miljard hinduer, och de
          flesta av dem bor i Indien. Där syns det idolbilder överallt på de många gudarna. Gudarna
          inom hinduismen är nämligen lite som superhjältar; som har särskilda krafter och beundras
          för sina goda egenskaper.
        </p>
        <p>I det här spelet får du lära dig om några av hindusimens populäraste gudar.</p>
        <p className="last-para">
          <small>
            <em>
              Aum sägs vara ljudet som hördes när världen skapades. Det är hinduismens heligaste
              symbol och sjungs av de som mediterar.
            </em>
          </small>
        </p>
        <ContactText />
      </div>
    );
  }
}

TitleText.defaultProps = {
  scrolled: false
};

TitleText.propTypes = {
  scrolled: PropTypes.bool
};

export { TitleText as default };
