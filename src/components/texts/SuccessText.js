import React from 'react';
import PropTypes from 'prop-types';
import global from '../utilities/globalStuff';
import color from '../utilities/colors';
import AnimatedCup from './illustrations/AnimatedCup';
import Cup from './illustrations/Cup';
import ContactText from './contact/ContactText';

class SuccessText extends React.Component {
  componentDidMount() {
    const div = document.querySelector('.info-box article .view div');
    const scrollbar = document.querySelector('.track-vertical');
    if (window.matchMedia('(max-width: 768px)').matches) {
      div.classList.remove('scrolled');
      scrollbar.style.visibility = 'hidden';
    } else {
      global.infobarFunc();
    }
  }
  render() {
    const { attemptsLog } = this.props;
    return (
      <div>
        <div className="success-text">
          <Cup fill={color.illustrationColor('orange')[0]} />
          <h1>Bra gjort!</h1>
          <p>Nu har alla gudarna fått sina favoritsaker.</p>
          <p>
            Enligt gammal hävd så har hinduismen 330 miljoner gudar. Det kommer sig av att allt på
            jorden, från människorna ner till minsta sten, betraktas som aspekter av det gudomliga.
            Gudarna som det berättas sagor om är egentligen färre än så, men det finns ändå många
            historier kvar att upptäcka.
          </p>
          <p>
            <em>Namaste!</em>
          </p>
          <div className="attempts-container">
            <span>
              <p>
                <small>Antal försök</small>
              </p>
              <h2>{attemptsLog}</h2>
            </span>
            <span>
              <p>
                <small>Bästa möjliga resultat</small>
              </p>
              <h2>30</h2>
            </span>
          </div>
          <ContactText />
        </div>
        <AnimatedCup fill={color.illustrationColor('orange')[0]} />
      </div>
    );
  }
}

SuccessText.defaultProps = {
  scrolled: false,
  attemptsLog: 0
};

SuccessText.propTypes = {
  scrolled: PropTypes.bool,
  attemptsLog: PropTypes.number
};

export { SuccessText as default };
