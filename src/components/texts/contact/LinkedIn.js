import React from 'react';

const LinkedIn = () => (
  <a
    href="https://www.linkedin.com/in/pontus-h-karlsson/"
    title="LinkedIn"
    target="_blank"
    rel="noopener noreferrer"
    alt="LinkedIn"
  >
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 6 36 36">
      <path
        id="linkedin"
        d="M6 10a4 4 0 0 1 4-4h28a4 4 0 0 1 4 4v28a4 4 0 0 1-4 4H10a4 4 0 0 1-4-4V10z"
      />
      <path
        d="M12 19h5v17h-5zM14.485 17h-.028C12.965 17 12 15.888 12 14.499 12 13.08 12.995 12 14.514 12c1.521 0 2.458 1.08 2.486 2.499C17 15.887 16.035 17 14.485 17zM36 36h-5v-9.1c0-2.197-1.225-3.697-3.191-3.697-1.502 0-2.313 1.012-2.707 1.99-.145.35-.102 1.318-.102 1.807v9h-5V19h5v2.616C25.721 20.5 26.85 19 29.738 19c3.578 0 6.261 2.25 6.261 7.273L36 36z"
        fill="#FFF"
      />
    </svg>
  </a>
);

export { LinkedIn as default };
