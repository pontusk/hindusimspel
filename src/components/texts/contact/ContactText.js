import React from 'react';
import PropTypes from 'prop-types';
import global from '../../utilities/globalStuff';
import LinkedIn from './LinkedIn';
import Behance from './Behance';
import GitLab from './GitLab';

class ContactText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div className="contact">
        <hr />
        <div className="my-info">
          <p className="last-para">
            <small>
              ©2018 <strong>Pontus Karlsson</strong>
            </small>
          </p>
          <div className="social-media">
            <LinkedIn />
            <Behance />
            <GitLab />
          </div>
        </div>
        <div className="attribution">
          <section>
            <p className="last-para">
              <small>Music by</small> <a href="https://www.bensound.com/">www.bensound.com</a>
            </p>
          </section>
          <section>
            <p className="no-margin">
              <small>Sound effects obtained from</small>
            </p>
            <ul>
              <li>
                <a href="https://www.zapsplat.com/">www.zapsplat.com</a>
              </li>
              <li>
                <a href="https://freesound.org/">freesound.org</a>
              </li>
              <li>
                <a href="https://www.pond5.com/">www.pond5.com</a>
              </li>
            </ul>
          </section>
        </div>
      </div>
    );
  }
}

ContactText.defaultProps = {
  scrolled: false
};

ContactText.propTypes = {
  scrolled: PropTypes.bool
};

export { ContactText as default };
