import React from 'react';
import global from '../../utilities/globalStuff';

class VishnuText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          <strong>Vishnu</strong> är beskyddaren som ingriper när onda krafter hotar världen. Han
          återföds då som en mänsklig hjälte som lever på jorden och ställer allt till rätta. Två
          exempel på det är Rama och Krishna: huvudpersonerna i de episka diktverken Ramayana och
          Mahabharata.
        </p>
        <p>Vishnu är gift med Lakshmi och bror till Parvati.</p>
        <hr />
        <small>
          <em>
            <p>
              Det var en gång en demon kallad Bali som var så mäktig att han erövrade hela världen.
              Gudarna var oroliga att Bali och hans demoner skulle erövra alla de tre världarna och
              bad Vishnu om hjälp.
            </p>
            <p>
              Vishnu återföddes då som dvärgen Vamana och växte upp i ett prästhushåll. En dag gick
              han till Balis tronsal och bad honom om en gåva: han ville ha så mycket land som han
              kunde kliva över med tre steg. Bali skrattade åt dvärgens önskemål och beviljade det
              med glädje.
            </p>
            <p>
              Så fort Bali hade gett sitt löfte så växte sig Vishnu jättelik och vann tillbaka hela
              världen åt gudarna genom att kliva över den i två steg. Med det tredje steget trampade
              han ner Bali till underjorden.
            </p>
          </em>
        </small>
      </div>
    );
  }
}

export { VishnuText as default };
