import React from 'react';
import global from '../../utilities/globalStuff';

class ShivaText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          <strong>Shiva</strong> är både dödens och livets gud. Det låter underligt, men det
          förklaras av att hinduerna tror på reinkarnation: det vill säga att döden innebär
          återfödelse till ett nytt liv. Shiva är stark och vildsint, men också en kärleksfull
          familjefar.
        </p>
        <p>Han är gift med Parvati och pappa till Ganesha.</p>
        <hr />
        <small>
          <em>
            <p>
              När gudarna kärnade det kosmiska mjölkhavet på jakt efter odödlighetens nektar så steg
              ett fasansfullt gift upp från havets botten.
            </p>
            <p>
              Något behövde göras åt det för att rädda världen, så Shiva handlade utan tanke på sin
              egen säkerhet och svalde allt gift i ett svep. För att hindra det från att sprida sig
              till andra delar av hans kropp klämde Parvati hårt om sin makes hals.
            </p>
            <p>Det är förklaringen till att Shiva är blå.</p>
          </em>
        </small>
      </div>
    );
  }
}

export { ShivaText as default };
