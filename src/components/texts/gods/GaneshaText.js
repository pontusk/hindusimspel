import React from 'react';
import global from '../../utilities/globalStuff';

class GaneshaText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          Elefantguden <strong>Ganesha</strong> är väldigt populär. Han är guden som tar bort alla
          hinder och hjälper människor att uppnå sina mål. Han är också visdomens gud.
        </p>
        <p>Ganeshas är son till Shiva och Parvati.</p>
        <hr />
        <small>
          <em>
            <p>
              Parvati skapade en gång en pojke av lera som hon gav liv. Hon döpte pojken till
              Ganesha och bad honom att vakta dörren medan hon tog ett bad.
            </p>
            <p>
              Då återvände Shiva efter en jakt. Ganesha visste inte vem det var och blockerade
              ingången som han hade blivit tillsagd att göra. Shiva, som inte visste att Ganesha var
              hans son, blev rasande och högg huvudet av pojken med sin treudd.
            </p>
            <p>
              När Parvati fick reda på vad som hänt var hennes vrede stor. Shiva lovade att rätt
              till sitt misstag och gick ner för berget i jakt på ett nytt huvud åt Ganesha.
            </p>
            <p>
              Det första djuret Shiva stötte på var en elefant, så han tog dess huvud och satte det
              på Ganeshas kropp. På så sätt fick Ganesha en elefants huvud och därför oändlig
              visdom.
            </p>
          </em>
        </small>
      </div>
    );
  }
}

export { GaneshaText as default };
