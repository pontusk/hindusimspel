import React from 'react';
import global from '../../utilities/globalStuff';

class KaliText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          <strong>Kali</strong> är en vildsintare version av Parvati. Hon är den stora modern och
          ondskans förgörare.
        </p>
        <p>
          I sin högsta form har hon tio armar som håller i föremål som förknippas med andra gudar.
          Det symboliserar att de olika gudarnas makt egentligen kommer från henne.
        </p>
        <hr />
        <small>
          <em>
            <p>
              Demonen Raktabeej terroriserade världen och de andra gudarna var förtvivlade. Ingen av
              dem visste hur han skulle besegras, eftersom varje blodsdroppe från honom som spilldes
              på marken blev en ny Raktabeej.
            </p>
            <p>
              Då gick Kali ut i strid och besegrade demonen och hans armé samtidigt som hon drack
              deras blod. På så sätt spilldes inte en enda droppe på marken.
            </p>
            <p>
              Efter striden var Kali berusad av blod och dansade så häftigt på slagfältet att
              världen höll på att rämna. För att lugna henne lade sig Shiva under hennes fötter så
              att hon trampade på hans bröst. När hon gjorde det bet sig Kali i tungan och slutade
              med sin dans.
            </p>
          </em>
        </small>
      </div>
    );
  }
}

export { KaliText as default };
