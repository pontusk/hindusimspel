import React from 'react';
import global from '../../utilities/globalStuff';

class ParvatiText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          <strong>Parvati</strong> är en mångsidig gudinna och har många egenskaper som förknippas
          med andra gudar. Mest känd är hon som kärlekens, moderskapets och den gudomliga styrkans
          gudinna.
        </p>
        <p className="last-para">
          Hon är gift med Shiva, mamma till Ganesha och syster till Vishnu.
        </p>
        <hr />
        <small>
          <em>
            <p>
              När Shivas första fru dog försjönk han i djup meditation och ondskans krafter kunde
              löpa amok över världen. De andra gudarna vädjade till universums kvinnliga urkraft att
              återfödas på nytt för att få Shiva att lämna sin grotta. Så föddes Parvati.
            </p>
            <p>
              För att vinna Shivas kärlek gick hon ut i skogen och mediterade utan mat och utan
              kläder. Han blev imponerad över hennes hängivenhet och de gifte sig med varandra.
            </p>
            <p>
              När buffeldemonen Mahishasura drev ut gudarna ur himmelriket så var Parvati den enda
              som kunde rädda dem. Hon tog emot välsignelser från Shiva, Brahma och Vishnu och
              återgick till sin ursprungliga form som universums kvinnliga urkraft. I den formen
              kallades hon Durga och hon besegrade demonen som alla trodde var oövervinnerlig.
            </p>
          </em>
        </small>
      </div>
    );
  }
}

export { ParvatiText as default };
