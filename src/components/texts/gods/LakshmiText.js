import React from 'react';
import global from '../../utilities/globalStuff';

class LakshmiText extends React.Component {
  componentDidMount() {
    global.infobarFunc();
  }
  render() {
    return (
      <div>
        <p>
          <strong>Lakshmi</strong> är rikedomens och lyckans gudinna. Hon är en populär husgud bland
          hinduer.
        </p>
        <p className="last-para">
          När hennes man Vishnu återföds på jorden för att bekämpa ondskan så återföds hon också:
          exempelvis som Ramas fru Sita eller Krishnas barndomsvän Radha.
        </p>
        <hr />
        <small>
          <em>
            <p>
              I begynnelsen fanns jordens gudinna som tog hand om allt levande. Gudarna beundrade
              hennes skönhet och demonerna ville ha hennes rikedomar; så det utkämpades ett krig som
              demonerna vann. Makten steg demonernas kung Bali åt huvudet.
            </p>
            <p>
              Vishnu överlistade Bali och gudarna fick makten över jorden. Men Vishnu ville inte
              härska utan lät Indra bli kung istället. Indra försummade sina plikter och förlustade
              sig i sin trädgård.
            </p>
            <p>
              Varken demoner eller gudar hade kunnat förvalta gudinnans gåvor, så hon försvann ner i
              det kosmiska mjölkhavet. Med henne försvann också allt gott och lyckligt från världen.
            </p>
            <p>
              Både gudarna och demonerna drabbades av panik och gjorde gemensam sak av att försöka
              få gudinnan tillbaka. De tillverkade en kosmisk smörkärna av ett berg, en
              jättesköldpadda och en jätteorm – och började kärna havet.
            </p>
            <p>
              Det tog eoner av arbete, med många missöden längs vägen, men till slut lät sig
              gudinnan bevekas och återvände i form av Lakshmi; sittandes på en daggstänkt lotus.
              Liv och glädje spred sig åter över världen.
            </p>
            <p>
              Eftersom Vishnu var den ende som inte hade missbrukat hennes gåvor, valde Lakshmi
              honom till sin make.
            </p>
          </em>
        </small>
      </div>
    );
  }
}

export { LakshmiText as default };
