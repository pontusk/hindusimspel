export default {
  skinColor(name) {
    let fill;
    let stroke;
    let noseStroke;
    let eyeFill;
    let eyeStroke;
    let noseFill;

    if (name === 'Shiva' || name === 'Kali' || name === 'Vishnu') {
      fill = '#61bce2';
      stroke = '#0090b7';
      noseStroke = '#026C9B';
      eyeFill = '#026C9B';
      eyeStroke = '#035470';
      noseFill = '#0099ce';
    } else if (name === 'Ganesha') {
      fill = '#B3C6C6';
      stroke = '#7E9899';
      noseStroke = '#026C9B';
      eyeFill = '#678689';
      eyeStroke = '#507272';
    } else if (name === 'Parvati') {
      fill = '#F98C3A';
      stroke = '#F15A24';
      noseStroke = '#E2752B';
      eyeFill = '#D64629';
      eyeStroke = '#B22A18';
    } else if (name === 'Lakshmi') {
      fill = '#F98C3A';
      stroke = '#F15A24';
      noseStroke = '#EA623D';
      eyeFill = '#FF5FA8';
      eyeStroke = '#D60578';
    }
    return [fill, stroke, noseStroke, eyeFill, eyeStroke, noseFill];
  },
  illustrationColor(illustration) {
    let fill;
    if (illustration === 'orange') {
      fill = 'rgb(255, 87, 52)';
    }
    return [fill];
  }
};
