import Sound from 'react-sound';

// Contains some lists of stuff to be kept in one place
export default {
  attrs: [
    'lotus',
    'trident',
    'axe',
    'snake',
    'skull',
    'money',
    'club',
    'bowl',
    'shell',
    'chakra',
    'rope',
    'drum'
  ],
  gods: () => {
    let godsArray;
    if (window.matchMedia('(max-width: 768px)').matches) {
      godsArray = ['Shiva', 'Parvati', 'Ganesha', 'Vishnu', 'Kali', 'Lakshmi'];
    } else {
      godsArray = ['Shiva', 'Parvati', 'Kali', 'Ganesha', 'Vishnu', 'Lakshmi'];
    }
    return godsArray;
  },
  infoTypes: function info() {
    const infoArray = [...this.attrs, 'god', 'closed', 'tutorial'];
    return infoArray;
  },
  infobarFunc: () => {
    const article = document.querySelector('.info-box article');
    const div = document.querySelector('.info-box article .view div');
    const scrollbar = document.querySelector('.track-vertical');
    const outer = article.clientHeight;
    const inner = div.scrollHeight;
    // Hide scrollbar when not needed and add margin so text isn't obscured when the scrollbar is present
    if (outer < inner) {
      div.className = 'scrolled';
      scrollbar.style.visibility = 'visible';
    } else {
      div.classList.remove('scrolled');
      scrollbar.style.visibility = 'hidden';
    }
    // Return to top when the page changes
    document.querySelector('.view').scrollTop = 0;
  }
};
