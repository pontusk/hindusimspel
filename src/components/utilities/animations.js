import { TimelineMax, Power1, TweenMax, Elastic, Power4, SteppedEase, Power2, Linear } from 'gsap';
import Draggable from '../../../node_modules/gsap/src/uncompressed/utils/Draggable';
import global from './globalStuff';

export default {
  // -------------- Idle animations --------------

  idleEyes() {
    // Makes the eyes blink
    const tl1 = new TimelineMax({
      delay: 0,
      repeatDelay: 10,
      repeat: -1
    });
    tl1.to('#eyes-default-closed', 0, {
      opacity: 0,
      delay: 0.2,
      repeat: 1,
      repeatDelay: Math.random() * 10,
      yoyo: true
    });

    // Makes the pupils move
    const tl2 = new TimelineMax({
      delay: 0,
      repeatDelay: Math.random() * 20,
      repeat: -1
    });
    tl2
      .to('#pupil-right, #pupil-left', 0.5, {
        x: 5,
        delay: Math.random() * 10
      })
      .to('#pupil-right, #pupil-left', 0.5, {
        x: 0,
        delay: Math.random() * 10
      })
      .to('#pupil-right, #pupil-left', 0.5, {
        x: -5,
        delay: Math.random() * 10
      })
      .to('#pupil-right, #pupil-left', 0.5, {
        x: 0,
        delay: Math.random() * 10
      });
  },
  idleSnake() {
    // Makes the snake's eyes blink
    const tl1 = new TimelineMax({ repeat: -1, repeatDelay: Math.random() * 10 });
    tl1
      .to('#snake-eyes-closed', 0, {
        opacity: 0,
        delay: 0
      })
      .to('#snake-eyes-open', 0, {
        opacity: 0,
        delay: 2
      })
      .to('#snake-eyes-closed', 0, {
        opacity: 1,
        delay: 0
      })
      .to('#snake-eyes-open', 0, {
        opacity: 1,
        delay: 0.2
      })
      .to('#snake-eyes-closed', 0, {
        opacity: 0,
        delay: 0
      });
    const tl2 = new TimelineMax({ repeat: -1, delay: 0, repeatDelay: Math.random() * 5 });
    tl2
      .set('#snake-tongue-inv, #snake-tongue-default', {
        opacity: 0
      })
      .set('#snake-tongue-short', {
        opacity: 1
      })
      .set('#snake-tongue-default', {
        opacity: 1,
        delay: 0.2
      })
      .set('#snake-tongue-short', {
        opacity: 0,
        delay: 0
      })
      .set('#snake-tongue-inv', {
        opacity: 1,
        delay: 0.2
      })
      .set('#snake-tongue-default', {
        opacity: 0,
        delay: 0
      })
      .set('#snake-tongue-default', {
        opacity: 1,
        delay: 0.2
      })
      .set('#snake-tongue-inv', {
        opacity: 0,
        delay: 0
      })
      .set('#snake-tongue-inv', {
        opacity: 1,
        delay: 0.2
      })
      .set('#snake-tongue-default', {
        opacity: 0,
        delay: 0
      })
      .set('#snake-tongue-default', {
        opacity: 1,
        delay: 0.2
      })
      .set('#snake-tongue-inv', {
        opacity: 0,
        delay: 0
      })
      .set('#snake-thongue-short', {
        opacity: 1,
        delay: 0.2
      })
      .set('#snake-tongue-default', {
        opacity: 0,
        delay: 0
      })
      .set('#snake-tongue-short', {
        opacity: 0,
        delay: 0.2
      });
  },
  idleChakra() {
    TweenMax.to('#chakra', 0.5, { repeat: -1, x: -576, ease: SteppedEase.config(3) });
  },

  // -------------- Triggered animations --------------

  wagFinger(name) {
    // Makes the finger wag
    const tl = new TimelineMax();
    if (name === 'Vishnu') {
      tl
        .to('#arm1-point', 0.5, {
          rotation: -7,
          transformOrigin: '10% 90%',
          ease: Power1.easeIn
        })
        .set('#arm1-point', {
          rotation: -7,
          transformOrigin: '10% 90%'
        })
        .to('#arm1-point', 0.8, {
          repeat: -1,
          yoyo: true,
          rotation: 5,
          transformOrigin: '10% 90%',
          ease: Power1.easeInOut
        });
    } else {
      tl
        .to('#arm1-point', 0.5, {
          rotation: 7,
          transformOrigin: '90% 90%',
          ease: Power1.easeIn
        })
        .set('#arm1-point', {
          rotation: 7,
          transformOrigin: '90% 90%'
        })
        .to('#arm1-point', 0.8, {
          repeat: -1,
          yoyo: true,
          rotation: -5,
          transformOrigin: '90% 90%',
          ease: Power1.easeInOut
        });
    }
  },

  wagFingerHandOnly(name) {
    // Makes the finger wag for characters with pointing hand in lower position
    const tl = new TimelineMax();
    if (name === 'Lakshmi') {
      tl
        .to('#hand-pointing', 0.5, {
          rotation: -20,
          transformOrigin: '50% 90%',
          ease: Power1.easeIn
        })
        .set('#hand-pointing', {
          rotation: -20,
          transformOrigin: '50% 90%'
        })
        .to('#hand-pointing', 0.8, {
          repeat: -1,
          yoyo: true,
          rotation: 5,
          transformOrigin: '50% 90%',
          ease: Power1.easeInOut
        });
    } else {
      tl
        .to('#hand-pointing', 0.5, {
          rotation: 20,
          transformOrigin: '50% 90%',
          ease: Power1.easeIn
        })
        .set('#hand-pointing', {
          rotation: 20,
          transformOrigin: '50% 90%'
        })
        .to('#hand-pointing', 0.8, {
          repeat: -1,
          yoyo: true,
          rotation: -5,
          transformOrigin: '50% 90%',
          ease: Power1.easeInOut
        });
    }
  },

  snout() {
    // Makes Ganesha's snout move
    TweenMax.to('#snout', 0.25, { x: -678.168, ease: SteppedEase.config(3) });
  },
  snoutReverse() {
    TweenMax.to('#snout', 0.25, { x: 0, ease: SteppedEase.config(3) });
  },
  // -------------- Draggable --------------

  draggableDrawer() {
    TweenMax.set('.drawer, .item-container', { left: '60vw' }); // offset the drawer to begin with
    Draggable.create('.item-container', {
      trigger: '.draggable-handle', // activate this only on the handle not to interfere with the draggable items
      type: 'x',
      edgeResistance: 0.95,
      bounds: '.drawerBounds',
      onClick: function clickToOpenDrawer() {
        const drawer = document.querySelector('.item-container');
        console.log(drawer._gsTransform);
        if (drawer._gsTransform.xPercent > -50) {
          TweenMax.to('.drawer, .item-container', 0.5, { x: '-85%' });
        } else {
          TweenMax.to('.drawer, .item-container', 0.5, { x: '0%' });
        }
      },
      onDragEnd: function dragToOpenDrawer() {
        // snap the drawer in place based on drag direction
        if (this.getDirection() === 'left') {
          TweenMax.to('.drawer, .item-container', 0.5, { x: '-85%' });
        } else {
          TweenMax.to('.drawer, .item-container', 0.5, { x: '0%' });
        }
      }
    });
  },

  draggableItems(
    thatSmileOn,
    thatSmileOff,
    thatPointOn,
    thatPointOff,
    thatAttr1On,
    thatAttr1Off,
    thatAttr2On,
    thatAttr2Off,
    thatAttr3On,
    thatAttr3Off,
    thatAttr4On,
    thatAttr4Off,
    thatAttr5On,
    thatAttr5Off,
    thatAttr6On,
    thatAttr6Off,
    thatAttr7On,
    thatAttr7Off,
    thatAttr8On,
    thatAttr8Off,
    thatAttr9On,
    thatAttr9Off,
    thatAttr10On,
    thatAttr10Off,
    thatInfoOn,
    thatSwitchSound,
    logAttempts,
    name
  ) {
    Draggable.create('.item', {
      onClick: function click() {
        const attr = this.target.className
          .split('-')
          .pop()
          .split(' ')
          .shift();
        // Activate info on the attribute that is clicked
        thatInfoOn(attr);

        // Open and close drawer on click
        if (window.matchMedia('(max-width: 768px)').matches) {
          const tl = new TimelineMax();
          tl
            .to('.item-container', 0.5, { x: '0%' })
            .to('.drawer', 0.5, { x: '0%' })
            .set('.item-container', { backgroundColor: '#470e00', borderColor: '#ff5734' })
            .set('.draggable-handle', { opacity: 1 })
            // hide the drawer div again
            .set('.drawer', { opacity: 0 });
        }
        thatSwitchSound('click');
      },
      onDrag: function drag() {
        // make the item smaller so it doesn't obstruct view
        TweenMax.to(this.target, 0.2, { scale: 0.5 });

        // variables for the different items
        const target = this.target.className;
        const snake = 'attr-snake item';
        const trident = 'attr-trident item';
        const axe = 'attr-axe item';
        const bowl = 'attr-bowl item';
        const chakra = 'attr-chakra item';
        const club = 'attr-club item';
        const lotus = 'attr-lotus item';
        const shell = 'attr-shell item';
        const drum = 'attr-drum item';
        const rope = 'attr-rope item';
        const money = 'attr-money item';
        const skull = 'attr-skull item';

        // make the container see through so we can avoid animating the target
        if (window.matchMedia('(max-width: 768px)').matches) {
          TweenMax.set('.item-container', {
            backgroundColor: 'transparent',
            borderColor: 'transparent'
          });
          TweenMax.set('.draggable-handle', { opacity: 0 });

          // show the drawer div underneath instead
          const tl = new TimelineMax();
          tl
            .set('.drawer', { opacity: 1 })
            // move the drawer out of the way
            .to('.drawer', 0.5, { x: '0%' });

          // move the items out of the way
          if (target === snake) {
            TweenMax.to(
              '.attr-trident, .attr-axe, .attr-bowl, .attr-chakra, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === trident) {
            TweenMax.to(
              '.attr-snake, .attr-axe, .attr-bowl, .attr-chakra, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === axe) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-bowl, .attr-chakra, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === bowl) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-chakra, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === chakra) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === club) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-chakra, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === lotus) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-chakra, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === shell) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-lotus, .attr-chakra, .attr-drum, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === drum) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-lotus, .attr-shell, .attr-chakra, .attr-rope, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === rope) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-chakra, .attr-money, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === money) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-chakra, .attr-skull',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          } else if (target === skull) {
            TweenMax.to(
              '.attr-snake, .attr-trident, .attr-axe, .attr-bowl, .attr-club, .attr-lotus, .attr-shell, .attr-drum, .attr-rope, .attr-money, .attr-chakra',
              0.5,
              {
                transform: 'translate3d(62vw, 0, 0)'
              }
            );
          }
        }

        // what happens when the item hovers over the god
        if (this.hitTest('#god', '50%')) {
          // depending on which god, we show different animations depending on what attributes are correct
          if (name === 'Shiva') {
            if (target === trident || target === drum || target === snake) {
              thatSmileOn();
              thatSwitchSound('male-approving');
            } else {
              thatPointOn();
              thatSwitchSound('disapproving');
            }
          } else if (name === 'Ganesha') {
            if (target === axe || target === rope || target === lotus) {
              thatSmileOn();
              thatSwitchSound('male-approving');
            } else if (target === bowl) {
              thatSmileOn();
              thatSwitchSound('ganesha-tasty');
            } else {
              thatPointOn();
              thatSwitchSound('disapproving');
            }
          } else if (name === 'Parvati') {
            if (
              target === chakra ||
              target === axe ||
              target === club ||
              target === lotus ||
              target === trident ||
              target === shell ||
              target === drum ||
              target === rope ||
              target === snake
            ) {
              thatSmileOn();
              thatSwitchSound('female-approving');
            } else {
              thatPointOn();
              thatSwitchSound('disapproving');
            }
          } else if (name === 'Lakshmi') {
            if (target === lotus || target === money) {
              thatSmileOn();
              thatSwitchSound('female-approving');
            } else {
              thatPointOn();
              thatSwitchSound('disapproving');
            }
          } else if (name === 'Kali') {
            if (
              target === axe ||
              target === club ||
              target === chakra ||
              target === trident ||
              target === shell ||
              target === skull ||
              target === lotus ||
              target === snake
            ) {
              thatSmileOn();
              thatSwitchSound('kali-approving');
            } else {
              thatPointOn();
              thatSwitchSound('kali-disapproving');
            }
          } else if (name === 'Vishnu') {
            if (target === chakra || target === club || target === shell || target === lotus) {
              thatSmileOn();
              thatSwitchSound('male-approving');
            } else {
              thatPointOn();
              thatSwitchSound('disapproving');
            }
          } // more gods here
        } else {
          setTimeout(() => {
            thatSwitchSound('none');
            thatSmileOff();
            thatPointOff();
          }, 2800);
        }
      },
      onDragEnd: function drop() {
        // Log the attempt
        logAttempts();
        // variables for the different items
        const target = this.target.className;
        const snake = 'attr-snake item';
        const trident = 'attr-trident item';
        const axe = 'attr-axe item';
        const bowl = 'attr-bowl item';
        const chakra = 'attr-chakra item';
        const club = 'attr-club item';
        const lotus = 'attr-lotus item';
        const shell = 'attr-shell item';
        const drum = 'attr-drum item';
        const rope = 'attr-rope item';
        const money = 'attr-money item';
        const skull = 'attr-skull item';

        const returnDrawer = () => {
          const tl = new TimelineMax();
          if (window.matchMedia('(max-width: 768px)').matches) {
            tl
              .to(this.target, 1, {
                scale: 0.05,
                opacity: 0,
                ease: Elastic.easeOut.config(2.5, 0.3)
              })
              .to('.item-container', 0, { x: '0%' })
              .to('.item', 0, { transform: 'translate3d(0, 0, 0)' })
              .to(this.target, 0, {
                scale: 1,
                opacity: 1
              })
              // reverse making the container see through
              .set('.item-container', { backgroundColor: '#470e00', borderColor: '#ff5734' })
              .set('.draggable-handle', { opacity: 1 })
              // hide the drawer div again
              .set('.drawer', { opacity: 0 });
          } else {
            tl
              .to(this.target, 1, {
                scale: 0.05,
                opacity: 0,
                ease: Elastic.easeOut.config(2.5, 0.3)
              })
              .to(this.target, 0.5, {
                scale: 1,
                opacity: 1,
                ease: Elastic.easeOut.config(0.5, 0.3)
              })
              .to(this.target, 0, { transform: 'translate3d(0, 0, 0)' }, '-=0.5');
          }
        };
        const returnItem = () => {
          const tl = new TimelineMax();
          if (window.matchMedia('(max-width: 768px)').matches) {
            tl
              .to('.item-container', 0.5, { x: '0%' })
              .to(this.target, 0, { transform: 'translate3d(0, 0, 0)' })
              .to('.item', 0, { transform: 'translate3d(0, 0, 0)' })
              // reverse making the container see through
              .set('.item-container', { backgroundColor: '#470e00', borderColor: '#ff5734' })
              .set('.draggable-handle', { opacity: 1 })
              // hide the drawer div again
              .set('.drawer', { opacity: 0 });
          } else {
            tl.to(this.target, 0.5, { transform: 'translate3d(0, 0, 0)' });
          }
        };
        setTimeout(() => {
          thatSwitchSound('none');
          thatSmileOff();
          thatPointOff();
        }, 2800);
        // what happens when the item is dropped
        if (this.hitTest('#god', '50%')) {
          // depending on what god, we either wink out the item or return it to the drawer
          if (name === 'Shiva') {
            if (target === snake || target === trident || target === drum) {
              // if it's the right item we wink out and add it to the model
              if (target === snake) {
                thatAttr1On();
              } else if (target === trident) {
                thatAttr2On();
              } else if (target === drum) {
                thatAttr3On();
              }
              returnDrawer();
            } else {
              // if it's the wrong item we return to drawer
              returnItem();
            }
          } else if (name === 'Ganesha') {
            if (target === bowl || target === axe || target === rope || target === lotus) {
              // if it's the right item we wink out and add it to the model
              if (target === bowl) {
                thatAttr1On();
              } else if (target === axe) {
                thatAttr2On();
              } else if (target === rope) {
                thatAttr3On();
              } else if (target === lotus) {
                thatAttr4On();
              }
              returnDrawer();
            } else {
              // if it's the wrong item we return to drawer
              returnItem();
            }
          } else if (name === 'Parvati') {
            if (
              target === chakra ||
              target === axe ||
              target === club ||
              target === lotus ||
              target === trident ||
              target === shell ||
              target === snake ||
              target === drum ||
              target === rope
            ) {
              // if it's the right item we wink out and add it to the model
              if (target === chakra) {
                thatAttr1On();
              } else if (target === axe) {
                thatAttr2On();
              } else if (target === club) {
                thatAttr3On();
              } else if (target === lotus) {
                thatAttr4On();
              } else if (target === trident) {
                thatAttr5On();
              } else if (target === shell) {
                thatAttr6On();
              } else if (target === snake) {
                thatAttr7On();
              } else if (target === drum) {
                thatAttr8On();
              } else if (target === rope) {
                thatAttr9On();
              }
              returnDrawer();
            } else {
              // if it's the wrong item we return to drawer
              returnItem();
            }
          } else if (name === 'Lakshmi') {
            if (target === lotus || target === money) {
              // if it's the right item we wink out and add it to the model
              if (target === lotus) {
                thatAttr1On();
              } else {
                thatAttr2On();
              }
              returnDrawer();
            } else {
              // if it's the wrong item we return to drawer
              returnItem();
            }
          } else if (name === 'Kali') {
            if (
              target === axe ||
              target === club ||
              target === chakra ||
              target === trident ||
              target === shell ||
              target === skull ||
              target === lotus ||
              target === snake
            ) {
              // if it's the right item we wink out and add it to the model
              if (target === axe) {
                thatAttr1On();
              } else if (target === club) {
                thatAttr2On();
              } else if (target === chakra) {
                thatAttr3On();
              } else if (target === trident) {
                thatAttr4On();
              } else if (target === shell) {
                thatAttr5On();
              } else if (target === skull) {
                thatAttr6On();
              } else if (target === lotus) {
                thatAttr7On();
              } else if (target === snake) {
                thatAttr8On();
              }
              returnDrawer();
            } else {
              // if it's the wrong item we return to drawer
              returnItem();
            }
          } else if (name === 'Vishnu') {
            if (target === chakra || target === club || target === shell || target === lotus) {
              if (target === chakra) {
                thatAttr1On();
              } else if (target === club) {
                thatAttr2On();
              } else if (target === shell) {
                thatAttr3On();
              } else if (target === lotus) {
                thatAttr4On();
              }
              returnDrawer();
            } else {
              returnItem();
            }
          } // more gods here
        } else {
          setTimeout(() => {
            thatSwitchSound('none');
            thatSmileOff();
            thatPointOff();
          }, 2800);
          // if the item is dropped outside the hitarea it's returned to the drawer
          returnItem();
        }
      }
    });
  },

  // -------------- Success Page --------------

  successEvent() {
    const tl = new TimelineMax();
    if (window.matchMedia('(max-width: 768px)').matches) {
      tl
        .set('.on-finished-god .info-box', {
          y: -200
        })
        .set('.logo', {
          scale: 0.05,
          opacity: 0
        })
        .to('.logo', 0, { opacity: 1, delay: 2.5 })
        .staggerTo(
          '.logo',
          0.8,
          {
            scale: 10,
            opacity: 0,
            ease: Power4.easeIn
          },
          0.5
        )
        .to('.on-finished-god .info-box', 0.5, {
          y: 0,
          ease: Power2.easeOut
        });
    } else {
      tl
        .set('.item', { transform: 'translate3d(0, 0, 0)' })
        .set('.on-finished-god .info-box', {
          y: -200
        })
        .set('.logo', {
          scale: 0.05,
          opacity: 0
        })
        .to('.logo', 0, { opacity: 1, delay: 2.5 })
        .staggerTo(
          '.logo',
          0.8,
          {
            scale: 10,
            opacity: 0,
            ease: Power4.easeIn
          },
          0.5
        )
        .to('.on-finished-god .info-box', 0.5, {
          y: 0,
          ease: Power2.easeOut
        });
    }
  },
  award() {
    TweenMax.set('#stars, #the-cup', { opacity: 0, scale: 0.001, transformOrigin: '50% 50%' });
    const tl = new TimelineMax();
    tl
      .to('#the-cup', 1.5, {
        delay: 1,
        opacity: 1,
        rotation: 720,
        scale: 1,
        transformOrigin: '50% 50%',
        ease: Elastic.easeOut.config(0.5, 0.4)
      })
      .add('start-label')
      .to('#stars', 0, { opacity: 1 })
      .to('#stars', 0.1, { scale: 1, transformOrigin: '50% 50%' })
      .add('star-label')
      .to(
        '#star-up',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 0, y: -100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-up-right',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 100, y: -100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-right',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 100, y: 0 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-down-right',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 100, y: 100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-down',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 0, y: 100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-down-left',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: -100, y: 100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-left',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: -100, y: 0 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-up-left',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: -100, y: -100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label'
      )
      .to(
        '#star-up, #star-up-right, #star-right, #star-down-right, #star-down, #star-down-left, #star-left, #star-up-left',
        0,
        { opacity: 1, x: 0, y: 0 }
      )
      .to('#star', 0, { scale: 0.001, transformOrigin: '50% 50%' })
      .to('#star', 0.1, { scale: 1, transformOrigin: '50% 50%' })
      .add('star-label2')
      .to(
        '#star-up',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 0, y: -100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-up-right',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 100, y: -100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-right',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 100, y: 0 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-down-right',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 100, y: 100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-down',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: 0, y: 100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-down-left',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: -100, y: 100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-left',
        0.5,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: -100, y: 0 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .to(
        '#star-up-left',
        0.75,
        {
          bezier: { type: 'thru', values: [{ x: 0, y: 0 }, { x: -100, y: -100 }] },
          opacity: 0,
          ease: Linear.easeIn
        },
        'star-label2'
      )
      .add('exit-label')
      .to('#cup', 0.75, { opacity: 0, delay: 0.5 }, 'exit-label')
      .set('#cup', { display: 'none' })
      .set('.success-text', { opacity: 0, display: 'block' })
      .add(global.infobarFunc)
      .add(() => {
        const track = document.querySelector('.track-vertical');
        if (track.style.visibility === 'visible') {
          document.querySelector('.view').scrollTop = 1;
        }
      })
      .to('.success-text', 0.75, { opacity: 1 });
  }
};
