import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import ContainerPresentation from './ContainerPresentation';

function mapStateToProps(state) {
  return {
    finishedGods: state.finishedGods,
    attemptsLog: state.attemptsLog,
    soundPlaying: state.soundPlaying,
    currentSound: state.currentSound
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}
const Container = connect(mapStateToProps, mapDispatchToProps)(ContainerPresentation);

export { Container as default };
