import React from 'react';
import { withRouter } from 'react-router';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';
import Button from '../general/Button';
import global from '../utilities/globalStuff';

class Navigation extends React.Component {
  handleClickBack() {
    this.props.switchSound('click');
    this.props.router.push('start');
  }
  handleClickSound() {
    const { soundPlaying, playSound, stopSound, switchSound } = this.props;
    if (soundPlaying) {
      stopSound();
    } else {
      playSound();
    }
    switchSound('click');
    ReactGA.event({
      category: 'Sound',
      action: 'Clicked sound button'
    });
  }
  handleClickInfo() {
    const { showInfo, updateState, switchSound } = this.props;
    if (showInfo === 'closed') {
      updateState({ showInfo: 'god' });
    } else {
      updateState({ showInfo: 'closed' });
    }
    switchSound('click');
  }
  render() {
    const infoButton = <span className="wider-button"></span>;
    const { name, isTitle } = this.props;
    return (
      <div className="navigation nav-btn">
        <nav>
          {!isTitle && (
            <Button title="" isSmaller alt="Back" onClick={() => this.handleClickBack()} />
          )}
          <Button title="" isSmaller alt="Sound" onClick={() => this.handleClickSound()} />
          {!isTitle && (
            <Button
              title={infoButton}
              isSmaller
              alt="Info"
              onClick={() => this.handleClickInfo()}
            />
          )}
        </nav>
        {!isTitle && <h1>{name}</h1>}
      </div>
    );
  }
}

Navigation.defaultProps = {
  currentSound: 'none',
  switchSound: () => console.log('prop is missing'),
  playSound: () => console.log('prop is missing'),
  stopSound: () => console.log('prop is missing'),
  soundPlaying: true,
  name: 'Shiva',
  updateState: () => console.log('prop is missing'),
  showInfo: 'closed',
  isTitle: false
};

Navigation.propTypes = {
  currentSound: PropTypes.string,
  switchSound: PropTypes.func,
  playSound: PropTypes.func,
  stopSound: PropTypes.func,
  soundPlaying: PropTypes.bool,
  name: PropTypes.oneOf(global.gods()),
  router: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  updateState: PropTypes.func,
  showInfo: PropTypes.oneOf(global.infoTypes()),
  isTitle: PropTypes.bool
};

const routedNavigation = withRouter(Navigation);

export { routedNavigation as default };
