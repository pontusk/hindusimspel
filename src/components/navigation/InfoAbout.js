import React from 'react';
import PropTypes from 'prop-types';
import Draggable from '../../../node_modules/gsap/src/uncompressed/utils/Draggable';
import Button from '../general/Button';
import TextBox from '../general/TextBox';
import global from '../utilities/globalStuff';
import ShivaText from '../texts/gods/ShivaText';
import ParvatiText from '../texts/gods/ParvatiText';
import GaneshaText from '../texts/gods/GaneshaText';
import VishnuText from '../texts/gods/VishnuText';
import LakshmiText from '../texts/gods/LakshmiText';
import KaliText from '../texts/gods/KaliText';
import TutorialText from '../texts/TutorialText';
import SnakeText from '../texts/attributes/SnakeText';
import TridentText from '../texts/attributes/TridentText';
import AxeText from '../texts/attributes/AxeText';
import BowlText from '../texts/attributes/BowlText';
import ChakraText from '../texts/attributes/ChakraText';
import ClubText from '../texts/attributes/ClubText';
import LotusText from '../texts/attributes/LotusText';
import ShellText from '../texts/attributes/ShellText';
import DrumText from '../texts/attributes/DrumText';
import RopeText from '../texts/attributes/RopeText';
import MoneyText from '../texts/attributes/MoneyText';
import SkullText from '../texts/attributes/SkullText';

class InfoAbout extends React.Component {
  static maxHeight() {
    // To fix vh bug with iOS we give the article a hight based on viewport height with javascript
    let maxHeight;
    if (window.matchMedia('(max-width: 768px)').matches) {
      maxHeight = `calc(${window.innerHeight}px - 100px - 10vw  - 8em)`;
    } else {
      maxHeight = 'calc(100vh - 10vw - 100px - 8em)';
    }
    return maxHeight;
  }
  componentDidMount() {
    // Disable draggable while info window is open
    let h;
    if (window.matchMedia('(max-width: 768px)').matches) {
      Draggable.get('.item-container').disable();

      // Stuff to deal with iOS
      h = `calc(${window.innerHeight}px - 100px - 10vw  - 8em)`;
      document.querySelector('.info-box article').style.height = h;
    }
  }
  componentWillUnmount() {
    // Enable draggable when info window is closed
    if (window.matchMedia('(max-width: 768px)').matches) {
      Draggable.get('.item-container').enable();
    }
  }
  handleClickClose() {
    this.props.switchSound('click');
    this.props.updateState({ showInfo: 'closed' });
  }
  handleClickForward() {
    // These if-statements serve to control how navigation works in the info window. They make it so the info about the god leads to the tutorial but not further, and the info about the attributes link to eachother but not to anything else.
    const { showInfo, updateState, switchSound } = this.props;
    if (showInfo === 'god') {
      updateState({ showInfo: 'tutorial' });
    } else if (global.attrs.indexOf(showInfo) > -1) {
      const index = global.attrs.indexOf(showInfo);
      updateState({ showInfo: global.attrs[index + 1] });
    }
    switchSound('click');
  }
  handleClickBack() {
    const { showInfo, updateState, switchSound } = this.props;
    if (showInfo === 'tutorial') {
      updateState({ showInfo: 'god' });
    } else if (global.attrs.indexOf(showInfo) > 0) {
      const index = global.attrs.indexOf(showInfo);
      updateState({ showInfo: global.attrs[index - 1] });
    }
    switchSound('click');
  }
  infoToShow() {
    const { name, showInfo } = this.props;
    let info;
    if (showInfo === 'god' && name === 'Shiva') {
      info = <ShivaText />;
    } else if (showInfo === 'god' && name === 'Parvati') {
      info = <ParvatiText />;
    } else if (showInfo === 'god' && name === 'Ganesha') {
      info = <GaneshaText />;
    } else if (showInfo === 'god' && name === 'Vishnu') {
      info = <VishnuText />;
    } else if (showInfo === 'god' && name === 'Lakshmi') {
      info = <LakshmiText />;
    } else if (showInfo === 'god' && name === 'Kali') {
      info = <KaliText />;
    } else if (showInfo === 'tutorial') {
      info = <TutorialText name={name} />;
    } else if (showInfo === 'snake') {
      info = <SnakeText showInfo={showInfo} />;
    } else if (showInfo === 'trident') {
      info = <TridentText showInfo={showInfo} />;
    } else if (showInfo === 'axe') {
      info = <AxeText showInfo={showInfo} />;
    } else if (showInfo === 'bowl') {
      info = <BowlText showInfo={showInfo} />;
    } else if (showInfo === 'chakra') {
      info = <ChakraText showInfo={showInfo} />;
    } else if (showInfo === 'club') {
      info = <ClubText showInfo={showInfo} />;
    } else if (showInfo === 'lotus') {
      info = <LotusText showInfo={showInfo} />;
    } else if (showInfo === 'shell') {
      info = <ShellText showInfo={showInfo} />;
    } else if (showInfo === 'drum') {
      info = <DrumText showInfo={showInfo} />;
    } else if (showInfo === 'rope') {
      info = <RopeText showInfo={showInfo} />;
    } else if (showInfo === 'money') {
      info = <MoneyText showInfo={showInfo} />;
    } else if (showInfo === 'skull') {
      info = <SkullText showInfo={showInfo} />;
    } else {
      info = 'something went wrong';
    }
    return info;
  }
  isHiddenBack() {
    // Hide button under certain conditions
    const { showInfo } = this.props;
    let b;
    if (showInfo === 'god' || showInfo === global.attrs[0]) {
      b = true;
    } else {
      b = false;
    }
    return b;
  }
  isHiddenClose() {
    const { showInfo } = this.props;
    let b;
    if (showInfo === 'god' && this.props.finishedGods[0] === undefined) {
      b = true;
    } else {
      b = false;
    }
    return b;
  }
  isHiddenForward() {
    // Hide button under certain conditions
    const { showInfo } = this.props;
    let b;
    if (showInfo === 'tutorial' || showInfo === global.attrs[global.attrs.length - 1]) {
      b = true;
    } else {
      b = false;
    }
    return b;
  }
  render() {
    return (
      <div className="info-container">
        <div className="info-box">
          <TextBox maxHeight={InfoAbout.maxHeight}>{this.infoToShow()}</TextBox>
          <div className="button-container nav-btn">
            <Button
              title=""
              isHidden={this.isHiddenBack()}
              isSmaller
              onClick={() => this.handleClickBack()}
              alt="Back"
            />
            <Button
              title=""
              isSmaller
              isHidden={this.isHiddenClose()}
              onClick={() => this.handleClickClose()}
              alt="Close"
            />
            <Button
              title=""
              isHidden={this.isHiddenForward()}
              isSmaller
              onClick={() => this.handleClickForward()}
              alt="Forward"
            />
          </div>
        </div>
      </div>
    );
  }
}

InfoAbout.defaultProps = {
  name: 'Shiva',
  updateState: () => console.log('prop is missing'),
  switchSound: () => console.log('prop is missing'),
  showInfo: 'god',
  finishedGods: []
};

InfoAbout.propTypes = {
  name: PropTypes.oneOf(global.gods()),
  updateState: PropTypes.func,
  switchSound: PropTypes.func,
  showInfo: PropTypes.oneOf(global.infoTypes()),
  finishedGods: PropTypes.arrayOf(PropTypes.string)
};

export { InfoAbout as default };
