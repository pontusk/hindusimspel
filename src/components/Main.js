import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import MainPresentation from './MainPresentation';

function mapStateToProps(state) {
  return {
    finishedGods: state.finishedGods,
    attemptsLog: state.attemptsLog,
    soundPlaying: state.soundPlaying,
    currentSound: state.currentSound
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}
const Main = connect(mapStateToProps, mapDispatchToProps)(MainPresentation);

export { Main as default };
