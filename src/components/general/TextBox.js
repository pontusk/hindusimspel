import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

const TextBox = ({ children, maxHeight }) => (
  <article>
    <Scrollbars
      renderTrackVertical={props => (
        <div
          {...props}
          style={{ width: 7, height: 'calc(100% - 4px)', position: 'absolute' }}
          className="track-vertical"
        />
      )}
      renderThumbVertical={props => (
        <div {...props} style={{ width: 7, position: 'relative' }} className="thumb-vertical" />
      )}
      renderView={props => <div {...props} className="view" />}
      autoHeight
      thumbMinSize={30}
      autoHeightMax={`${maxHeight()}`}
    >
      {children}
    </Scrollbars>
  </article>
);

TextBox.defaultProps = {
  children: 'something is missing',
  maxHeight: '100%'
};

TextBox.propTypes = {
  children: PropTypes.node,
  maxHeight: PropTypes.func
};

export { TextBox as default };
