import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ title, isInline, isSmaller, onClick, alt, isHidden }) => (
  <button
    className={`btn ${isInline ? 'btn-inline' : ''} ${isSmaller ? 'btn-smaller' : ''} ${
      isHidden ? 'hidden' : ''
    }`}
    onClick={onClick}
    alt={alt}
    disabled={isHidden && true}
  >
    <span>{title}</span>
  </button>
);

Button.defaultProps = {
  alt: '',
  title: undefined,
  isInline: false,
  isSmaller: false,
  isHidden: false,
  onClick: () => console.log('prop is missing')
};

Button.propTypes = {
  alt: PropTypes.string,
  title: PropTypes.node,
  isInline: PropTypes.bool,
  isSmaller: PropTypes.bool,
  isHidden: PropTypes.bool,
  onClick: PropTypes.func
};

export { Button as default };
