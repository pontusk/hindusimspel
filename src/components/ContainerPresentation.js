import React from 'react';
import PropTypes from 'prop-types';
import Sound from 'react-sound';
import theme from '../sounds/theme.mp3';
import click from '../sounds/click.mp3';

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.handleFinishedPlaying = this.handleFinishedPlaying.bind(this);
  }
  soundStatus() {
    const { soundPlaying } = this.props;
    let status;
    if (soundPlaying) {
      status = Sound.status.PLAYING;
    } else {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  clickPlay() {
    const { soundPlaying, currentSound } = this.props;
    let status = Sound.status.STOPPED;
    if (currentSound === 'click' && soundPlaying) {
      status = Sound.status.PLAYING;
    } else if (currentSound === 'none') {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  handleFinishedPlaying() {
    this.props.switchSound('none');
  }
  render() {
    return (
      <div style={{ height: '100%' }}>
        {this.props.children}
        <Sound url={theme} playStatus={this.soundStatus()} />
        <Sound
          url={click}
          autoLoad
          playStatus={this.clickPlay()}
          onFinishedPlaying={this.handleFinishedPlaying}
        />
      </div>
    );
  }
}

Container.defaultProps = {
  currentSound: 'none',
  switchSound: () => console.log('prop is missing'),
  children: <div />,
  soundPlaying: true
};

Container.propTypes = {
  currentSound: PropTypes.string,
  switchSound: PropTypes.func,
  children: PropTypes.node,
  soundPlaying: PropTypes.bool
};
export { Container as default };
