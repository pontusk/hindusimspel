// The only components with state are Main and Start. They pass their state down as props.
// The Redux store only stores the finishedGods array and the attemptsLog, which keeps track of progress, and soundPlaying which controls the sound globally.
// The methods for changing state are mostly in the Drawer component.
// The animations are defined in a separate file but called on mount in the concerned components.
// For example: the idle animation for the eyes is called when the closed eyes component is mounted.

import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import ReactGA from 'react-ga';
import store, { history } from '../store';
import Main from './Main';
import Start from './startpage/Start';
import Title from './startpage/Title';
import Container from './Container';
import global from './utilities/globalStuff';

class App extends React.Component {
  static titlePage(props) {
    return <Title {...props} />;
  }
  static startPage(props) {
    return <Start {...props} gods={global.gods()} />;
  }
  static shivaPage(props) {
    return <Main {...props} name="Shiva" />;
  }
  static vishnuPage(props) {
    return <Main {...props} name="Vishnu" />;
  }
  static lakshmiPage(props) {
    return <Main {...props} name="Lakshmi" />;
  }
  static kaliPage(props) {
    return <Main {...props} name="Kali" />;
  }
  static ganeshaPage(props) {
    return <Main {...props} name="Ganesha" />;
  }
  static parvatiPage(props) {
    return <Main {...props} name="Parvati" />;
  }
  static container(props) {
    return <Container {...props} />;
  }
  static fireTracking() {
    ReactGA.pageview(window.location.hash);
  }
  render() {
    return (
      <Provider store={store}>
        <Router history={history} onUpdate={App.fireTracking}>
          <Route path="/" component={App.container}>
            <IndexRoute component={App.titlePage} />
            <Route path="/start" component={App.startPage} />
            <Route path="/shiva" component={App.shivaPage} />
            <Route path="/vishnu" component={App.vishnuPage} />
            <Route path="/lakshmi" component={App.lakshmiPage} />
            <Route path="/kali" component={App.kaliPage} />
            <Route path="/ganesha" component={App.ganeshaPage} />
            <Route path="/parvati" component={App.parvatiPage} />
          </Route>
        </Router>
      </Provider>
    );
  }
}

export { App as default };
