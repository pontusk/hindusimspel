import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import Sound from 'react-sound';
import Draggable from '../../node_modules/gsap/src/uncompressed/utils/Draggable';
import animate from './utilities/animations';
import Logo from './general/Logo';
import Button from './general/Button';
import global from './utilities/globalStuff';
import gong from '../sounds/gong.mp3';

class OnFinishedGod extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gongPlaying: true
    };
    this.handleFinishedPlaying = this.handleFinishedPlaying.bind(this);
  }
  componentDidMount() {
    animate.successEvent();
    if (window.matchMedia('(max-width: 768px)').matches) {
      Draggable.get('.item-container').disable();
    }
    // When the game finishes we add the god to the finishedGods array through Redux,
    // when all are finished we clear the array.
    this.props.addGod(this.props.name);
  }
  componentWillUnmount() {
    if (window.matchMedia('(max-width: 768px)').matches) {
      Draggable.get('.item-container').enable();
    }
  }
  handleClick() {
    this.props.switchSound('click');
    this.props.router.push('start');
  }
  gongPlay() {
    let status = Sound.status.PLAYING;
    if (!this.state.gongPlaying) {
      status = Sound.status.STOPPED;
    }
    return status;
  }
  handleFinishedPlaying() {
    this.setState({ gongPlaying: false });
  }
  render() {
    const { soundPlaying } = this.props;
    return (
      <section className="on-finished-god">
        <div className="logo-container">
          <Logo withoutText />
          <Logo withoutText />
          <Logo withoutText />
        </div>
        <div className="info-box">
          <div>
            <Button isInline onClick={() => this.handleClick()} title="Fortsätt" />
          </div>
        </div>
        {soundPlaying && (
          <Sound
            url={gong}
            autoPlay
            playStatus={this.gongPlay()}
            onFinishedPlaying={this.handleFinishedPlaying}
          />
        )}
      </section>
    );
  }
}

OnFinishedGod.propTypes = {
  router: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

OnFinishedGod.defaultProps = {
  soundPlaying: false,
  switchSound: () => console.log('prop is missing'),
  name: 'Shiva',
  addGod: () => console.log('prop is missing'),
  finishedGods: []
};

OnFinishedGod.propTypes = {
  soundPlaying: PropTypes.bool,
  switchSound: PropTypes.func,
  name: PropTypes.oneOf(global.gods()),
  addGod: PropTypes.func,
  finishedGods: PropTypes.arrayOf(PropTypes.string)
};

const routedOnFinishedGod = withRouter(OnFinishedGod);

export { routedOnFinishedGod as default };
