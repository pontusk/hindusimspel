export function addGod(name) {
  return {
    type: 'ADD_GOD',
    name
  };
}

export function removeGod(name) {
  return {
    type: 'REMOVE_GOD',
    name
  };
}

export function clearGods(name) {
  return {
    type: 'CLEAR_GODS',
    name
  };
}

export function incrementAttempts() {
  return {
    type: 'INCREMENT_ATTEMPTS'
  };
}

export function clearAttempts() {
  return {
    type: 'CLEAR_ATTEMPTS'
  };
}

export function playSound() {
  return {
    type: 'PLAY_SOUND'
  };
}

export function stopSound() {
  return {
    type: 'STOP_SOUND'
  };
}

export function switchSound(sound) {
  return {
    type: 'SWITCH_SOUND',
    sound
  };
}
