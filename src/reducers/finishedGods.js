function finishedGods(state = [], action) {
  switch (action.type) {
    case 'ADD_GOD':
      return [...state, action.name];
    case 'REMOVE_GOD':
      return state.filter(name => name !== action.name);
    case 'CLEAR_GODS':
      return [];
    default:
      return state;
  }
}

export { finishedGods as default };
