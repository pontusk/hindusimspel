import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import finishedGods from './finishedGods';
import attemptsLog from './attemptsLog';
import soundPlaying from './soundPlaying';
import currentSound from './currentSound';

const rootReducer = combineReducers({
  finishedGods,
  attemptsLog,
  soundPlaying,
  currentSound,
  routing: routerReducer
});

export { rootReducer as default };
