function currentSound(state = 'none', action) {
  switch (action.type) {
    case 'SWITCH_SOUND':
      return action.sound;
    default:
      return state;
  }
}

export { currentSound as default };
