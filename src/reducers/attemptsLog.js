function attemptsLog(state = 0, action) {
  switch (action.type) {
    case 'INCREMENT_ATTEMPTS':
      return state + 1;
    case 'CLEAR_ATTEMPTS':
      return 0;
    default:
      return state;
  }
}

export { attemptsLog as default };
