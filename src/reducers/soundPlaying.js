function soundPlaying(state = false, action) {
  switch (action.type) {
    case 'PLAY_SOUND':
      return true;
    case 'STOP_SOUND':
      return false;
    default:
      return state;
  }
}

export { soundPlaying as default };
