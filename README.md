# Aum

This is a game designed to teach children about some of the hindu gods. The language used is Swedish.

[aum-spelet.se](https://aum-spelet.se)

It's a responsive web app made with [React](https://reactjs.org/), using [Redux](https://redux.js.org/) for state management and [GreenSock](https://greensock.com/) for animations.

## Other dependencies

* [Babel](https://babeljs.io/)
* [Sass](https://sass-lang.com/)
* [Webpack](https://webpack.js.org/)
* [ESLint](https://eslint.org/)
* [Prettier](https://github.com/prettier/prettier)
* [React Router](https://reacttraining.com/react-router/)
* [React Sound](https://github.com/leoasis/react-sound)
* [React Custom Scrollbars](https://github.com/malte-wessel/react-custom-scrollbars)
* [React Favicon](https://www.npmjs.com/package/react-favicon)

## Purpose

This game was made as a personal project to improve my coding skills and showcase what I can do.

## Author

* **Pontus Karlsson** – [hire me](https://www.linkedin.com/in/pontus-h-karlsson/)

## License

This project is licensed under the Apache license – see the [LICENSE](LICENSE) file for details.

## Attributions

Music by [www.bensound.com](https://www.bensound.com/)

Sound effects obtained from

* [www.zapsplat.com](https://www.zapsplat.com/)
* [freesound.org](https://freesound.org/)
* [www.pond5.com](https://www.pond5.com/)
